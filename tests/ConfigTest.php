<?php

namespace Yab\Test;

use Yab\Core\Config;

class ConfigTest extends TestCase {
	
	public function testGlobal() {
		
		$globalInstance = Config::getGlobal();

		$this->assertTrue($globalInstance instanceof Config);

	}
	
	public function testStaticSetGet() {
		
		Config::set('paramA', 'A');

		$this->assertEquals(Config::get('paramA'), 'A');

	}
	
	public function testFusion() {
		
		$configA = new Config(array('paramA' => 'A'));
		$configB = new Config(array('paramB' => 'B'));
		
		$configA->fusion($configB);
		
		$this->assertEquals($configA->getParam('paramB'), $configB->getParam('paramB'));

	}
	
	public function testDigConfigDotParams() {
		
		$params = array(
			'paramA.subParamA.subSubParamA' => 'A',
			'paramB' => 'B',
			'paramC' => 'C',
		);
		
		$diggedParams = array(
			'paramA' => array('subParamA' => array('subSubParamA' => 'A')),
			'paramB' => 'B',
			'paramC' => 'C',
		);
		
		$config = new Config($params);
		
		$getParams = $config->getParams();
		
		$this->assertEquals($getParams, $diggedParams);
		
	}
	
	public function testGetParams() {
		
		$params = array(
			'paramA' => 'A',
			'paramB' => 'B',
			'paramC' => 'C',
		);
		
		$config = new Config($params);
		
		$getParams = $config->getParams();
		
		$this->assertEquals($getParams, $params);
		
	}
	
	public function testGetParam() {
		
		$params = array(
			'paramA' => 'A',
			'paramB' => 'B',
			'paramC' => 'C',
		);
		
		$config = new Config($params);
		
		foreach($params as $param => $value) 
			$this->assertEquals($config->getParam($param), $value);
		
	}
	
	public function testSynthax() {
		
		$init = array(
			'params.subparams.paramA' => 'A',
			'params.subparams.paramB' => 'B',
			'params.subparams.paramC' => 'C',
		);
		
		$config = new Config($init);
		
		$params = $config->getParam('params');

		$this->assertTrue(is_array($params));
		$this->assertTrue(isset($params['subparams']));
		$this->assertEquals(count($params['subparams']), count($init));

		$paramC = $config->getParam('params.subparams.paramC');

		$this->assertEquals($paramC, 'C');
	}
	
	public function testFusionSynthax() {
		
		$initA = array(
			'params.subparams.paramA' => 'A',
			'params.subparams.paramB' => 'B',
			'params.subparams.paramC' => 'C',
		);
		
		$initB = array(
			'params.subparams.paramA' => 'A',
			'params.subparams.paramD' => 'D',
			'params.subparams.paramC' => 'X',
		);
		
		$configA = new Config($initA);
		$configB = new Config($initB);
		
		$configA->fusion($configB);

		$params = $configA->getParam('params');

		$this->assertTrue(is_array($params));
		$this->assertTrue(isset($params['subparams']));
		$this->assertEquals(count($params['subparams']), 4);

		$paramC = $configA->getParam('params.subparams.paramC');

		$this->assertEquals($paramC, 'X');

	}
	
	public function testSetParam() {
		
		$init = array(
			'params.subparams.paramA' => 'A',
			'params.subparams.paramB' => 'B',
			'params.subparams.paramC' => 'C',
		);
		
		$config = new Config($init);
		
		$config->setParam('newParam', 'P1');
		$config->setParam('params.subparams.paramA', 'A2');

		$params = $config->getParam('params');

		$paramA = $config->getParam('params.subparams.paramA');

		$this->assertEquals($paramA, 'A2');

		$newParam = $config->getParam('newParam');
	
		$this->assertEquals($newParam, 'P1');
		
	}
	
}