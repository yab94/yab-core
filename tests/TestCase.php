<?php

namespace Yab\Test;

use Yab\Core\Config;
use Yab\Core\Database;

use Yab\Test\ModelTest\Customer;
use Yab\Test\ModelTest\Bill;
use Yab\Test\ModelTest\Group;

class TestCase extends \PHPUnit\Framework\TestCase {

	protected $useDatabase = false;
	protected $database = 'testing';
	
	protected function setUp() {
		
		if(!$this->useDatabase)
			return;
			
		$database = new Database('sqlite::memory:');
		
		Database::setPool($database);

		$db = Database::getPool();
		
		$db->statement('
			CREATE TABLE IF NOT EXISTS `customer` (
				`customerId` INTEGER PRIMARY KEY AUTOINCREMENT,
				`first_name` TEXT NULL,
				`last_name` TEXT NULL
			);
		')->execute();

		$db->statement('
			CREATE TABLE IF NOT EXISTS  `bill` (
				`billId` INTEGER PRIMARY KEY AUTOINCREMENT,
				`foreignCustomerId` INTEGER NULL,
				`label` TEXT NULL,
				`amount` float DEFAULT NULL,
				`date` datetime DEFAULT NULL
			);
		')->execute();
		
		$db->statement('
			CREATE TABLE IF NOT EXISTS  `group` (
				`groupId` INTEGER PRIMARY KEY AUTOINCREMENT,
				`name` TEXT NULL
			);
		')->execute();

		$db->statement('
			CREATE TABLE IF NOT EXISTS  `customer_group` (
				`throughCustomerId` INTEGER,
				`throughGroupId` INTEGER,
				PRIMARY KEY (`throughCustomerId`, `throughGroupId`)
			);
		')->execute();

		 $db->query('PRAGMA foreign_keys = ON');
		 $db->query('PRAGMA page_size = 4096');
		 $db->query('PRAGMA cache_size = 10000');
		 $db->query('PRAGMA synchronous = OFF');
		 $db->query('PRAGMA journal_mode = MEMORY');

	}	
	
	protected function beforeEach() {
		
		if(!$this->useDatabase)
			return;
		
		$db = Database::getPool();

		$db->statement('TRUNCATE TABLE `customer`')->execute();
		$db->statement('TRUNCATE TABLE `bill`')->execute();
		$db->statement('TRUNCATE TABLE `group`')->execute();
		$db->statement('TRUNCATE TABLE `customer_group`')->execute();

	}
	
	protected function afterAll() {
		
		if(!$this->useDatabase)
			return;
		
	}
    
    protected function createXCustomers($x) {

        for($i = 1; $i <= $x; $i++) {

            Customer::instance()
                ->set('first_name', 'first_name_'.$i)
                ->set('last_name', 'last_name_'.$i)
                ->save();
        
        }

        return $this;

    }
    
    protected function createXGroups($x) {

        for($i = 1; $i <= $x; $i++) {

            Group::instance()
                ->set('name', 'name_'.$i)
                ->save();
        
        }

        return $this;

    }
	
}