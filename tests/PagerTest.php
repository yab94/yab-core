<?php

namespace Yab\Test;

use Yab\Core\Pager;
use Yab\Core\Response;
use Yab\Core\Request;

use Yab\Test\ModelTest\Customer;

class PagerTest extends TestCase {
	
	protected $useDatabase = true;
    
    public function testPaginate() {

        Customer::new(['first_name' => 'Charles'])->save();
        Customer::new(['first_name' => 'Yann'])->save();
        Customer::new(['first_name' => 'Albert'])->save();

        $customers = Customer::all();

        $request = new Request('GET', '/customers?p=2&pp=1');
        $response = new Response(200);

        $pager = new Pager($request, $response);

        $pager->paginate($customers);

        $name = $customers->fetchValue('first_name')->next();
        
        $this->assertEquals($name, 'Yann');
        $this->assertEquals($pager->getPage(), 2);
        $this->assertEquals($pager->getPerPage(), 1);
        $this->assertEquals($pager->getRowCount(), 3);

    }
    
    public function testPerPage() {

        $request = new Request('GET', '/customers');
        $response = new Response(200);

        $pager = new Pager($request, $response);

        $perPage = $pager->getPerPage();

        $this->assertEquals($perPage, 25);

        $perPages = $pager->getPerPages();

        $this->assertTrue(is_array($perPages));

        for($i = 0; $i < 56; $i++)
            Customer::new(['first_name' => 'Charles'.$i])->save();

        $pager->paginate(Customer::all());

        $this->assertFalse(in_array(56, $pager->getPerPages()));
        
        $pager->withPerPagesMax(true);

        $this->assertTrue(in_array(56, $pager->getPerPages()));

    }
    
    public function testPages() {

        $request = new Request('GET', '/customers?p=3&pp=4');
        $response = new Response(200);

        $pager = new Pager($request, $response);

        for($i = 0; $i < 56; $i++)
            Customer::new(['first_name' => 'Charles'.$i])->save();

        $pager->paginate(Customer::all());

        $this->assertEquals($pager->getPage(), 3);
        $this->assertEquals($pager->getPreviousPage(), 2);
        $this->assertEquals($pager->getNextPage(), 4);
        $this->assertEquals($pager->getLastPage(), ceil(56/4));
        $this->assertEquals($pager->getFirstPage(), 1);
        
        $pager->withPerPagesMax(true);

        $this->assertTrue(in_array(56, $pager->getPerPages()));

    }
   
}