<?php

namespace Yab\Test;

use Yab\Core\Request;
use Yab\Core\Response;

class HttpTest extends TestCase {

    public function testBody() {
        
        $response = new Response(200);

        $response->setBody('123');

        $this->assertEquals($response->getBody(), '123');

        $response->setBody('456');

        $this->assertEquals($response->getBody(), '456');

        $response->appendBody('789');

        $this->assertEquals($response->getBody(), '456789');

    }
    
    public function testHeaders() {
        
        $response = new Response(200);
        
        $this->assertTrue(is_array($response->getHeaders()));

        $response->setHeader('Content-Type', 'text/html');

        $this->assertEquals($response->getHeader('Content-Type'), 'text/html');

        try {

            $response->getHeader('Unknown-Header');

            $this->assertTrue(false);

        } catch(\Exception $e) {

            $this->assertTrue(true);

        }
 
    }
   
}