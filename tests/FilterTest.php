<?php

namespace Yab\Test;

use Yab\Core\Filter;
use Yab\Core\Request;
use Yab\Core\Response;
use Yab\Core\Logger;

use Yab\Test\ModelTest\Customer;

class FilterTest extends TestCase {
	
	protected $useDatabase = true;
    
    public function testFilter() {

        Customer::new(['first_name' => 'Charles'])->save();
        Customer::new(['first_name' => 'Yann'])->save();
        Customer::new(['first_name' => 'Albert'])->save();

        $customers = Customer::all();

        $request = new Request('POST', '/customers');

        $request->setBodyParams(['filter1' => 'Yann']);

        $response = new Response(200);

        $filter = new Filter($request, $response);
        $filter->setAttribute('method', 'POST');
        $filter->addText('filter1')->set('filter', 'first_name');

        $filter->filter($customers);

        $name = $customers->fetchValue('first_name')->next();
        
        $this->assertEquals($name, 'Yann');

    }
    
    public function testFilterFromCookies() {

        Customer::new(['first_name' => 'Charles'])->save();
        Customer::new(['first_name' => 'Yann'])->save();
        Customer::new(['first_name' => 'Albert'])->save();

        $customers = Customer::all();

        $request = new Request('GET', '/customers');
        $request->setCookie('cufilter1', 'Yann');

        $response = new Response(200);

        $filter = new Filter($request, $response, 'cu');
        $filter->setAttribute('method', 'POST');
        $filter->addText('filter1')->set('filter', 'first_name');

        $filter->filter($customers);

        $name = $customers->fetchValue('first_name')->next();
        
        $this->assertEquals($name, 'Yann');

    }
    
    public function testFilterNoName() {

        Customer::new(['first_name' => 'Charles'])->save();
        Customer::new(['first_name' => 'Yann'])->save();
        Customer::new(['first_name' => 'Albert'])->save();

        $customers = Customer::all();

        $request = new Request('POST', '/customers');

        $request->setBodyParams(['first_name' => 'Yann']);

        $response = new Response(200);

        $filter = new Filter($request, $response);
        $filter->setAttribute('method', 'POST');
        $filter->addText('first_name');

        $filter->filter($customers);

        $name = $customers->fetchValue('first_name')->next();
        
        $this->assertEquals($name, 'Yann');

    }
    
    public function testCookie() {

        $request = new Request('POST', '/customers');
        $request->setBodyParams(['filter1' => 'Yann']);

        $response = new Response(200);

        $filter = new Filter($request, $response, 'cu');
        $filter->setAttribute('method', 'POST');
        $filter->addText('filter1')->set('filter', 'first_name');

        $customers = Customer::all();
        $filter->filter($customers);
        
        $this->assertEquals($response->getCookie('cufilter1'), 'Yann');

    }
    
    public function testReset() {

        $request = new Request('POST', '/customers');
        $request->setBodyParams(['filter1' => 'Yann']);

        $response = new Response(200);

        $filter = new Filter($request, $response, 'cu');
        $filter->setAttribute('method', 'POST');
        $filter->addText('filter1')->set('filter', 'first_name');

        $customers = Customer::all();
        $filter->filter($customers);

        $reset = new Request('GET', $filter->getResetUrl());

        $this->assertEquals($reset->getQueryParams(), ['filter1' => '']);

    }
    
    public function testCallback() {

        Customer::new(['first_name' => 'Charles'])->save();
        Customer::new(['first_name' => 'Yann'])->save();
        Customer::new(['first_name' => 'Albert'])->save();

        $request = new Request('POST', '/customers');
        $request->setBodyParams(['filter1' => 'Yann']);

        $response = new Response(200);

        $filter = new Filter($request, $response, 'cu');
        $filter->setAttribute('method', 'POST');
        $filter->addText('filter1')->set('callback', function($statement, $filter1) {
            $statement->whereEq('first_name', $filter1);
        });

        $customers = Customer::all();
        
        $filter->filter($customers);

        $name = $customers->fetchValue('first_name')->next();
        
        $this->assertEquals($name, 'Yann');

    }
    
    public function testArray() {

        Customer::new(['first_name' => 'Charles'])->save();
        Customer::new(['first_name' => 'Yann'])->save();
        Customer::new(['first_name' => 'Albert'])->save();

        $request = new Request('POST', '/customers');
        $request->setBodyParams(['filter1' => 'Yann']);

        $response = new Response(200);

        $filter = new Filter($request, $response, 'cu');
        $filter->setAttribute('method', 'POST');
        $filter->addText('filter1')->set('filter', ['first_name', 'last_name']);

        $customers = Customer::all();
        
        $filter->filter($customers);

        $name = $customers->fetchValue('first_name')->next();
        
        $this->assertEquals($name, 'Yann');

    }
    
    public function testBadType() {

        Customer::new(['first_name' => 'Charles'])->save();
        Customer::new(['first_name' => 'Yann'])->save();
        Customer::new(['first_name' => 'Albert'])->save();

        $request = new Request('POST', '/customers');
        $request->setBodyParams(['filter1' => 'Yann']);

        $response = new Response(200);

        $filter = new Filter($request, $response, 'cu');
        $filter->setAttribute('method', 'POST');
        $filter->addSubmit('filter1')->set('filter', 'first_name');

        $customers = Customer::all();
        
        $filter->filter($customers);

        $name = $customers->fetchValue('first_name')->next();
        
        $this->assertEquals($name, 'Charles');

    }
    
    public function testNoValue() {

        Customer::new(['first_name' => 'Charles'])->save();
        Customer::new(['first_name' => 'Yann'])->save();
        Customer::new(['first_name' => 'Albert'])->save();

        $request = new Request('POST', '/customers');

        $response = new Response(200);

        $filter = new Filter($request, $response, 'cu');
        $filter->setAttribute('method', 'POST');
        $filter->addText('filter1')->set('filter', 'first_name');

        $customers = Customer::all();
        
        $filter->filter($customers);

        $name = $customers->fetchValue('first_name')->next();
        
        $this->assertEquals($name, 'Charles');

    }
   
}