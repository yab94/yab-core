<?php

namespace Yab\Test;

use Yab\Core\Logger;
use Yab\Core\Database;
use Yab\Core\Event;
use Yab\Core\Statement;
use Yab\Test\ModelTest\Customer;
use Yab\Test\ModelTest\Bill;
use Yab\Test\ModelTest\Group;
use Yab\Test\ModelTest\NoPrimary;
use Yab\Test\ModelTest\NoTable;

class ModelTest extends TestCase {
	
	protected $useDatabase = true;
	
	public function testIssetHasManyToMany() {
        
        $customer = Customer::new(['first_name' => 'yann'])->save();
  
        $this->createXGroups(3);

        foreach(Group::select() as $group) $this->assertFalse($customer->issetGroups($group));
        
        $customer->addGroups(Group::select());

        foreach(Group::select() as $group) $this->assertTrue($customer->issetGroups($group));
      
	}
	
	public function testSelectWithHasManyToMany() {
        
        $this->createXCustomers(10);
        $this->createXGroups(10);
        
        Foreach(Customer::select() as $customer)
            $customer->addGroups(Group::select());

        $queryCount = 0;

        Event::addListener(Database::EVENT_QUERY, function($query) use(&$queryCount) { $queryCount++; }, 'queryCount');

        foreach(Customer::with('groups') as $customer)
            $this->assertEquals(count($customer->groups), 10);

        foreach(Group::with('customers') as $group) 
            $this->assertEquals(count($group->customers), 10);

        Event::removeListener(Database::EVENT_QUERY, 'queryCount');

        $this->assertEquals($queryCount, 6);
 
	}

    public function testInstance() {
        
        $this->assertTrue(Customer::instance() instanceof Customer);
    
    }

    public function testMapping() {
        
        $datas = ['first_name' => 'yann'];

        $customer = new Customer();

        $customer->feed($datas);

        $this->assertEquals($customer->getFirstName(), $datas['first_name']);
       
        $newName = "gdsgdsgsd";

        $customer->setFirstName($newName);

        $this->assertEquals($customer->first_name, $newName);
        $this->assertEquals($customer->firstName, $newName);
    
        $attributes = $customer->getAttributes(['firstName'], false);

        $this->assertEquals($attributes, ['first_name' => $newName]);
    
        $attributes = $customer->getAttributes(['firstName'], true);

        $this->assertEquals($attributes, ['firstName' => $newName]);

    }

    public function testBadImplementation() {
        
        try {

            NoTable::table();

            $this->assertTrue(false);

        } catch(\Exception $e) {

            $this->assertTrue(true);

        }
        
        try {

            $model = new NoPrimary();

            $this->assertTrue(false);

        } catch(\Exception $e) {

            $this->assertTrue(true);

        }
    
    }
    
    public function testDatabase() {
        
        $this->assertTrue(Customer::database() instanceof Database);
    
    }
    
    public function testTable() {
        
        $this->assertEquals(Customer::table(), 'customer');
    
    }
    
    public function testPrimary() {
        
        $this->assertEquals(Customer::primary(), array('customerId'));
    
    }
    
    public function testArrayAccess() {
        
        $customer = Customer::new(['first_name' => 'yann'])->save();
    
        $this->assertEquals($customer['first_name'], 'yann');

        $customer['first_name'] = 'yann2';
    
        $this->assertEquals($customer['first_name'], 'yann2');
    
        $this->assertTrue(isset($customer['first_name']));
    
        $this->assertFalse(isset($customer['first_name2']));

        unset($customer['first_name']);

        $this->assertFalse(isset($customer['first_name']));

    }
    
    public function testFeed() {

        $datas = ['first_name' => 'yann'];

        $customer = new Customer();

        $customer->feed($datas);

        $this->assertEquals($customer->first_name, 'yann');

    }
    
    public function testFind() {
		
		$customer = new Customer();
		$customer->set('first_name', 'Yann');
		$customer->set('last_name', 'BELLUZZI');
		$customer->forceInsert();
		
        $id = $customer->get('customerId');
        
		$this->assertTrue(is_numeric($id));
        
        $yann = Customer::find($id);
        
		$this->assertEquals($customer->last_name, $yann->last_name);
        
    }
    
    public function testFindBy() {
		
		$customer = new Customer();
		$customer->set('first_name', 'YannUnik1234');
		$customer->set('last_name', 'BELLUZZI');
		$customer->forceInsert();
        
        $yann = Customer::findBy(array('first_name' => 'YannUnik1234'));
        
		$this->assertEquals($customer->last_name, $yann->last_name);
        
    }
    
    public function testCheckUnicity() {
		
        $uniqueValue = 'YannUnik1234gdgfdhg4dsg';
        
		$customer = new Customer();
		$customer->set('first_name', $uniqueValue);
		$customer->set('last_name', $uniqueValue);
        
		$this->assertTrue($customer->checkUnicity('first_name', $uniqueValue));
        
		$customer->forceInsert();
        
		$customer = new Customer();
		$this->assertFalse($customer->checkUnicity('first_name', $uniqueValue));
        
    }

	public function testForceInsert() {

		$customer = new Customer();
		$customer->set('first_name', 'Yann');
		$customer->set('last_name', 'BELLUZZI');
		$customer->forceInsert();
		
        $id = $customer->get('customerId');
        
		$this->assertTrue(is_numeric($id));
        
        $yann = Customer::find($id);
        
		$this->assertEquals($customer->get('first_name'), $yann->get('first_name'));
		$this->assertEquals($customer->get('last_name'), $yann->get('last_name'));
        
		$customers = Customer::select();
		
		$this->assertEquals(1, count($customers));
		
	}
	
	public function testForceUpdate() {

		$customer = new Customer();
		$customer->set('first_name', 'Yann');
		$customer->set('last_name', 'BELLUZZI');
		$customer->forceInsert();
		
        $id = $customer->get('customerId');
        
        $yann = Customer::findBy(['customerId' => $id]);
        
        $yann->set('first_name', 'Yannou')->forceUpdate(); 
        
        $yannou = Customer::findBy(['customerId' => $id]);
        
		$this->assertEquals($yannou->get('first_name'), $yann->get('first_name'));
		
	}
	
	public function testExists() {
		
		$customer = new Customer();
        
		$customer->set('first_name', 'Yann');
		$customer->set('last_name', 'BELLUZZI');
		
        $this->assertFalse($customer->exists());
        
        $customer->save();
		
        $this->assertTrue($customer->exists());
		
	}
	
	public function testSave() {
		
		$customer = new Customer();
		$customer->set('first_name', 'Yann');
		$customer->set('last_name', 'BELLUZZI');
		$customer->save(); // insert
		
        $id = $customer->get('customerId');
        
        $yann = Customer::find($id);
        
        $yann->set('first_name', 'Yannou')->save(); // update
        
        $yannou = Customer::find($id);
        
		$this->assertEquals($yannou->get('first_name'), $yann->get('first_name'));
		
	}
	
	public function testSet() {
		
		$customer = new Customer();
        
        $value = 'yann';
        
		$customer->set('first_name', $value);
        
        $this->assertEquals($customer->getFirst_name(), $value);
        $this->assertEquals($customer->first_name, $value);
        $this->assertEquals($customer->get('first_name'), $value);

	}
	
	public function testAccessors() {
		
		$customer = new Customer();
        
        $value = 'yann';
        
		$customer->set('first_name', $value);
        
        $this->assertEquals($customer->getFirst_name(), $value);
        $this->assertEquals($customer->first_name, $value);
        $this->assertEquals($customer->get('first_name'), $value);
        
        try {
            
            $customer->get('first_NAme'); // case sensitive attributes
            
            $this->assertTrue(false);
            
        } catch(\Exception $e) {
            
            $this->assertTrue(true);
            
        }

        unset($customer->first_name);
        
        $this->assertFalse(isset($customer->first_name));

	}
	
	public function testHasMany() {
		
		$customer = new Customer();
		$customer->set('first_name', 'Yann');
		$customer->set('last_name', 'BELLUZZI');
		$customer->save();
        
        $billA = new \Yab\Test\ModelTest\Bill();
        $billA->set('label', 'billA');
        $billA->set('amount', '199.99');
        $billA->set('date', date('Y-m-d H:i:s'));
        $billA->save();
        
        $billB = new \Yab\Test\ModelTest\Bill();
        $billB->set('label', 'billB');
        $billB->set('amount', '299.99');
        $billB->set('date', date('Y-m-d H:i:s'));
        $billB->save();
                
        // Event::addListener(Database::EVENT_QUERY, function($query) { echo $query.PHP_EOL; }, 'sqlListener');

        $customer->bills->add($billA)->add($billB);
        
        $bills = $customer->bills;
        
        $this->assertEquals(2, count($bills));

        $customer->removeBills($billA);

        $bills = $customer->bills;
        
		$this->assertEquals(1, count($bills));

        $bill = $bills->next();
        
		$this->assertEquals($bill->amount, $billB->amount);

        $customer->getBills()->removeAll();
        
		$this->assertEquals(0, count($customer->bills));

	}
	
	public function testRemoveAllHasMany() {
		
		$customer = new Customer();
		$customer->set('first_name', 'Yann');
		$customer->set('last_name', 'BELLUZZI');
		$customer->save();
        
        $billA = new \Yab\Test\ModelTest\Bill();
        $billA->set('label', 'billA');
        $billA->set('amount', '199.99');
        $billA->set('date', date('Y-m-d H:i:s'));
        $billA->save();
        
        $billB = new \Yab\Test\ModelTest\Bill();
        $billB->set('label', 'billB');
        $billB->set('amount', '299.99');
        $billB->set('date', date('Y-m-d H:i:s'));
        $billB->save();
                
        // Event::addListener(Database::EVENT_QUERY, function($query) { echo $query.PHP_EOL; }, 'sqlListener');

        $customer->bills->add($billA)->add($billB);
        
        $customer->getBills()->whereEq('label', 'billB')->removeAll();
        
        $this->assertEquals(1, count($customer->bills));

	}
	
	public function testHasOne() {
		
		$customer = new Customer();
		$customer->set('first_name', 'Yann');
		$customer->set('last_name', 'BELLUZZI');
		$customer->save();
        
        $billA = new \Yab\Test\ModelTest\Bill();
        $billA->set('label', 'billA');
        $billA->set('amount', '199.99');
        $billA->set('date', date('Y-m-d H:i:s'));
        $billA->save();
        
        $billA->setCustomer($customer);
        
		$this->assertEquals($customer->id, $billA->customer->id);
		$this->assertEquals($customer->id, $billA->getCustomer()->id);
		$this->assertEquals($customer->id, $billA->get('customer')->id);
        
        $billA->removeCustomer();
        
        try {
            
            $billA->customer;
            
            $this->assertTrue(false);
            
        } catch(\Exception $e) {
            
            $this->assertTrue(true);
            
        }

	}
	
	public function testHasManyToMany() {

        $vars = array('A', 'B', 'C');
        
        foreach($vars as $var) {
		
            $group = Group::instance()->set('name', 'group_'.$var)->save();
            
            $customer = Customer::instance()->set('first_name', 'first_name_'.$var)->set('last_name', 'last_name_'.$var)->save();
                
        }

        foreach(Customer::select() as $customer) {
            
            foreach(Group::select() as $group) {
                
                $customer->addGroups($group);
                
            }
            
        }

        // Event::addListener(Database::EVENT_QUERY, function($query) { echo $query.PHP_EOL; }, 'sqlListener');

        foreach(Customer::select() as $customer) {
            
            $this->assertEquals(count($customer->groups), count(Group::select()));
            
        }
        
        $customerA = Customer::findBy(array('first_name' => 'first_name_A'));
        $groupB = Group::findBy(array('name' => 'group_B'));
        
        $customerA->removeGroups($groupB);
         
        $this->assertEquals(count($customerA->getGroups()), count(Group::select()) - 1);
        
        $customerA->removeAllGroups();
         
        $this->assertEquals(count($customerA->get('groups')), 0);

	}
	
	public function testHasManyToManyRemoveAll() {

        $vars = array('A', 'B', 'C');
        
        foreach($vars as $var) 
            Group::instance()->set('name', 'group_'.$var)->save();

        $customerA = Customer::new(['first_name' => 'first_name_A', 'last_name' => 'last_name_A'])->save();

        foreach(Group::all() as $group) 
            $customerA->addGroups($group);

        $this->assertEquals(count($customerA->groups), count($vars));

        $groups = $customerA->getGroups()->whereEq('name', 'group_B');

        $this->assertEquals(count($groups), 1);

        $groups->removeAll();

        $this->assertEquals(count($groups), 0);

        $this->assertEquals(count($customerA->getGroups()), 2);
	}
	
	public function testSelectWithHasOne() { 
        
        $vars = array('A', 'B', 'C');
        $vars2 = array(100, 2000, 30000); 

        foreach($vars as $var) {

            $customer = Customer::instance()->set('first_name', $var)->set('last_name', 'last_name_'.$var)->save();

            foreach($vars2 as $var2) { 
            
                $bill = Bill::instance()->set('label', $var)->set('amount', $var2);
                
                $customer->addBills($bill);

            }
                
        }
        
        $queryCount = 0;
        
        Event::addListener(Database::EVENT_QUERY, function($query) use(&$queryCount) { 
            $queryCount++; 
            // echo $query.PHP_EOL;
        }, 'queryCount');

        foreach(Bill::with('customer') as $bill) 
            $this->assertEquals($bill->customer->first_name, $bill->label);
        
        Event::removeListener(Database::EVENT_QUERY, 'queryCount');

        $queryCount = 0;
        
        Event::addListener(Database::EVENT_QUERY, function($query) use(&$queryCount) { 
            $queryCount++; 
            // echo $query.PHP_EOL;
        }, 'queryCount');

        foreach(Bill::with('customer')->whereEq('amount', 2000) as $bill) 
            $this->assertEquals($bill->customer->first_name, $bill->label);
        
        Event::removeListener(Database::EVENT_QUERY, 'queryCount');

        $this->assertEquals($queryCount, 3);
        
	}
	
	public function testSelectWithHasMany() {
        
        $vars = array('A', 'B', 'C');
        $vars2 = array(100, 2000, 30000); 
        
        foreach($vars as $var) {

            $customer = Customer::instance()->set('first_name', 'first_name_'.$var)->set('last_name', 'last_name_'.$var)->save();
        
            foreach($vars2 as $var2) {
                 
                $customer->addBills(Bill::instance()->set('label', 'bill_'.$var)->set('amount', $var2));
                
            }
                
        }
        
        $queryCount = 0;
        
        Event::addListener(Database::EVENT_QUERY, function($query) use(&$queryCount) { $queryCount++; }, 'queryCount');

        foreach(Customer::with('bills') as $customer) {

            $this->assertTrue(is_array($customer->bills));
            $this->assertEquals(count($customer->bills), count($vars2));

        }
        
        Event::removeListener(Database::EVENT_QUERY, 'queryCount');

        $this->assertEquals($queryCount, 3);

    }
	
	public function testSelectWithNested() {
    
        Event::addListener(Database::EVENT_QUERY, function($query) use(&$queryCount) { $queryCount++; }, 'queryCount');
        
        $this->createXCustomers(10);
        $this->createXGroups(10);
        
        Foreach(Customer::select() as $customer) {
            $customer->addGroups(Group::select());
            $customer->addBills(Bill::instance()->setAmount(rand(100, 200)));
        }

        $queryCount = 0;

        foreach(Group::with(array('customers', 'customers.bills')) as $group) {

            $this->assertEquals(count($group->customers), 10);

            foreach($group->customers as $customer)
                $this->assertEquals(count($customer->bills), 1);

        }
        
        Event::removeListener(Database::EVENT_QUERY, 'queryCount');

        $this->assertEquals($queryCount, 5);

	}
	
	public function testAttributes() {
    
        Event::addListener(Database::EVENT_QUERY, function($query) use(&$queryCount) { $queryCount++; }, 'queryCount');
        
        $this->createXCustomers(10);
        $this->createXGroups(10);
        
        foreach(Customer::select() as $customer) {
            $customer->addGroups(Group::select());
            $customer->addBills(Bill::instance()->setAmount(rand(100, 200)));
        }

	}
    
}