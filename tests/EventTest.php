<?php

namespace Yab\Test;

use Yab\Core\Event;

class EventTest extends TestCase {
    
    const EVENT_NAME = 'fake_testing_event';

    public function setUp() {

        parent::setUp();

        Event::removeAllListeners();

    }

    public function testFire() {
        
        $trigger = 0;
        $inc = 5;

        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; });

        Event::fire(self::EVENT_NAME, $inc);

        $this->assertEquals($trigger, $inc);

    }

    public function testAddListener() {
        
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener1');
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener2');
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener3');
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener3');

        $listeners = Event::getListeners(self::EVENT_NAME);

        $this->assertEquals(count($listeners), 3);

    }

    public function testRemoveListener() {
        
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener1');
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener2');
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener3');
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener3');

        $listeners = Event::getListeners(self::EVENT_NAME);

        $this->assertEquals(count($listeners), 3);

        Event::removeListener(self::EVENT_NAME, 'test_listener2');

        $listeners = Event::getListeners(self::EVENT_NAME);

        $this->assertEquals(count($listeners), 2);

    }

    public function testRemoveAllListener() {
        
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener1');
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener2');
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener3');
        Event::addListener(self::EVENT_NAME, function($inc) use(&$trigger) { $trigger += $inc; }, 'test_listener3');

        $listeners = Event::getListeners(self::EVENT_NAME);

        $this->assertEquals(count($listeners), 3);

        Event::removeAllListeners();

        $listeners = Event::getListeners(self::EVENT_NAME);

        $this->assertEquals(count($listeners), 0);

    }

}