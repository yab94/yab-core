<?php

namespace Yab\Test\CommandTest;

use Yab\Core\Command;

class MyJob extends Command {

    public function index($param1, $param2) {

        $this->write('success with '.$param1." ".$param2);

    }

}