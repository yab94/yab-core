<?php

namespace Yab\Test\ModelTest;

use Yab\Core\Model;

class NoPrimary extends Model {
	
    static protected $table = 'group';
    static protected $sequence = true;
	
}