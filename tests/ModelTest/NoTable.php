<?php

namespace Yab\Test\ModelTest;

use Yab\Core\Model;

class NoTable extends Model {
	
    static protected $primary = array('billId');
    static protected $sequence = true;
	
}