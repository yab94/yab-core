<?php

namespace Yab\Test;

use Yab\Core\Config;
use Yab\Core\Request;
use Yab\Core\Response;
use Yab\Core\Controller;
use Yab\Core\Application;
use Yab\Core\View;
use Yab\Core\Logger;

use Yab\Test\ControllerTest\Products;

class ControllerTest extends TestCase {

    public function testController() {

        $request = new Request('PUT', '/api/v1/products');
        $response = new Response(404);

        $config = new Config([
            'application.baseUrl' => '',
            'routes.put' => ['/api/v1/products' => Products::class.'.action'],
            'logger.display' => 0,
        ]);
  
        $application = new Application();
        $application->configure($config);
        $response = $application->route($request, $response);

        $this->assertEquals($response->getCode(), 200);
        $this->assertEquals($response->getBody(), "beforeactionafter");

    }

    public function testViews() {
        
        $request = new Request('PUT', '/api/v1/products');

        $response = new Response(200);
        $controller = new Products($request, $response);
        $controller->actionNoLayout();
        $this->assertEquals($response->getBody(), '<p>foobar</p>');

        $response = new Response(200);
        $controller = new Products($request, $response);
        $controller->actionLayout();
        $this->assertTrue((bool) preg_match('#.*<html.+<p>foobar</p>.+</html>.*#is', $response->getBody()));

        $response = new Response(200);
        $controller = new Products($request, $response);
        $controller->actionNormal();
        $this->assertTrue((bool) preg_match('#.*<html.+<p>foobar</p>.+</html>.*#is', $response->getBody()));

        $request->setHeader('X-Requested-With', 'xmlhttprequest');
        $response = new Response(200);
        $controller = new Products($request, $response);
        $controller->actionNormal();
        $this->assertEquals($response->getBody(), '<p>foobar</p>');

    }

    public function testFlashMessages() {
        
        $messageVar = Config::get('application.flashMessageVar', 'FLASH_MESSAGE_VAR');

        $request = new Request('PUT', '/api/v1/products');
        $response = new Response(200);
        $controller = new Products($request, $response);
        
        $controller->addFlashMessage('OK');
        $controller->addFlashMessage('KO', 'warning');
        $controller->addFlashMessage('Fail!', 'warning');
        $this->assertEquals($response->getCookie($messageVar), json_encode(['success' => ['OK'], 'warning' => ['KO', 'Fail!']]));

        $controller->clearFlashMessages();
        $this->assertEquals($response->getCookie($messageVar), json_encode([]));

    }

}