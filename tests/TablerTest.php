<?php

namespace Yab\Test;

use Yab\Core\Tabler;

use Yab\Test\ModelTest\Customer;

class TablerTest extends TestCase {
	
	protected $useDatabase = true;
    
    public function testGlobal() {

        $this->createXCustomers(10);

        $customers = Customer::all();

        $tabler = new Tabler($customers);

        $tabler->setTemplate(__DIR__.'/templates/tabler.php');

        $headers = ['firstName', 'lastName'];

        $tabler->setHeaders($headers);
        $tabler->setCallback(function($customer) use($headers) {
            return $customer->getAttributes($headers);
        });

        $render = $tabler->getRender();

        $result = implode(' ', $tabler->getHeaders()).PHP_EOL;

        foreach($tabler->getDatas() as $line)
            $result .= implode(' ', $line).PHP_EOL;

        $this->assertEquals($render, $result);

    }
   
}