<?php

namespace Yab\Test;

use Yab\Core\Cache;

class CacheTest extends TestCase {

    public function testIssetUnsetSetGet() {
 
        $this->assertFalse(Cache::isset('key'));
 
        Cache::set('key', 'value');
 
        $this->assertTrue(Cache::isset('key'));
 
        $this->assertEquals(Cache::get('key'), 'value');

        Cache::unset('key');
 
        $this->assertFalse(Cache::isset('key'));
 
    }

    public function testCache() {
 
        $key = Cache::cache('value2');
 
        $this->assertEquals(Cache::get($key), 'value2');
 
    }

}