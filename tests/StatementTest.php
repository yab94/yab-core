<?php

namespace Yab\Test;

use Yab\Core\Logger;
use Yab\Core\Database;
use Yab\Core\Event;
use Yab\Core\Statement;
use Yab\Test\ModelTest\Customer;
use Yab\Test\ModelTest\Bill;
use Yab\Test\ModelTest\Group;

class StatementTest extends TestCase {
	
	protected $useDatabase = true;
	
	public function testSelect() {
		
		$stmt = Database::getPool()->statement('SELECT * FROM customer');
		
		$this->assertEquals($stmt->fromAlias(), 'customer');
		
		$stmt->alias('f');

		$this->assertEquals($stmt->getSql(), 'SELECT * FROM customer f');
		$this->assertEquals($stmt->fromAlias(), 'f');
		
	}
	
	public function testExecute() {
		
		$stmt = Database::getPool()->statement('SELECT * FROM customer;');
		
		$this->assertFalse($stmt->isExecuted());
        
		$stmt->execute();
		
		$this->assertTrue($stmt->isExecuted());
		
	}
	
	public function testReset() {
		
		$stmt = Database::getPool()->statement('SELECT * FROM customer;');
		
		$this->assertFalse($stmt->isExecuted());
        
		$stmt->execute();
		
		$this->assertTrue($stmt->isExecuted());
        
		$stmt->reset();
		
		$this->assertFalse($stmt->isExecuted());
		
	}
	
	public function testDeleteAll() {

		$customer = Customer::new(['first_name' => 'customerA'])->save();

		$amounts = range(30, 50);

		foreach($amounts as $amount)
			$customer->addBills(Bill::new(['amount' => $amount])->save());

		$this->assertEquals(count(Bill::all()), count($amounts));
		$this->assertEquals(count($customer->bills), count($amounts));

		$bills = $customer->getBills()->whereLt('amount', 40);
		
		$this->assertEquals(count($bills), 10);

		$bills->deleteAll();
		
		$this->assertEquals(count($bills), 0);

		$this->assertEquals(count(Bill::all()), 11);
	
	}
	
	public function testFetchValue() {

		$amount = rand(100, 10000);

		Bill::new(['amount' => $amount])->save();

		// Basic collection return instance of Model
		$bill = Bill::all()->next();
		$this->assertTrue($bill instanceof Bill);
			
		// Simple value synthax
		$bill = Bill::all()->fetchValue('amount')->next();
		$this->assertEquals($bill, $amount);

		// Nested callbacks
		$bill = Bill::all()
			->fetchValue('amount')
			->fetchValue(function($amount) {return $amount / 2; })
			->fetchValue(function($amountBy2) { return $amountBy2 / 3; })
			->next();

		$this->assertEquals($bill, ((($amount) / 2) / 3));
		
	} 
	
}