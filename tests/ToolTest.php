<?php

namespace Yab\Test;

use Yab\Core\Tool;
use Yab\Core\Config;
use Yab\Core\Application;
use Yab\Test\ControllerTest\Products;

class ToolTest extends TestCase {
    
    public function testResolve() {

        $config = Config::getGlobal()->fusion(new Config([
            'application.baseUrl' => '',
            'http.put' => ['/api/v1/products' => Products::class.'.action'],
        ]));

        $url = Tool::resolve('PUT', Products::class.'.action');

        $this->assertEquals($url, '/api/v1/products');

    }
   
}