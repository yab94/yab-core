<?php

namespace Yab\Test\ControllerTest;

use Yab\Core\Controller;

class Products extends Controller {

    public function before() {

        $this->response->appendBody('before');

    }

    public function action() {

        $this->response->appendBody('action');
        
    }

    public function actionNoLayout() {

        $this->render(dirname(__DIR__).'/templates/view.php', ['var' => 'foobar'], false);
        
    }

    public function actionLayout() {

        $this->render(dirname(__DIR__).'/templates/view.php', ['var' => 'foobar'], true);
        
    }

    public function actionNormal() {

        $this->render(dirname(__DIR__).'/templates/view.php', ['var' => 'foobar']);
        
    }

    public function after() {

        $this->response->appendBody('after');
        
    }

}