<?php

namespace Yab\Test;

use Yab\Core\Config;
use Yab\Core\Application;
use Yab\Core\Request;
use Yab\Core\Response;
use Yab\Core\View;

class ApplicationTest extends TestCase {

    public function testAddNamespace() {

        $config = new Config([
            'logger.default.display' => 1,
        ]);
        $application = new Application();
        $application->configure($config);
        $application->addNamespace('MyOtherNamespace', __DIR__.'/AutoloadTest');

        $class = new \MyOtherNamespace\MyDir\MyOtherClass();

        $this->assertTrue($class instanceof \MyOtherNamespace\MyDir\MyOtherClass);

    }

    public function testDebugBar() {

        $request = new Request('DELETE', '/api/v1/fake');
        $response = new Response(200);

        $config = new Config([
            'logger.default.display' => 1,
        ]);
        $application = new Application();
        $application->configure($config);
        $debugBar = $application->debugBar($request, $response);

        $this->assertTrue($debugBar instanceof View);

    }

    public function testAutoload() {

        $config = new Config([
            'autoload' => [
                'MyNamespace' => __DIR__.'/AutoloadTest',
            ],
            'logger.default.display' => 1,
        ]);
        $application = new Application();
        $application->configure($config);

        $class = new \MyNamespace\MyDir\MyClass();

        $this->assertTrue($class instanceof \MyNamespace\MyDir\MyClass);

    }

    public function testContext() {

        $this->assertTrue(Application::setContext());
        $this->assertFalse(Application::setContext());

        $this->assertTrue(Application::unsetContext());
        $this->assertFalse(Application::unsetContext());

    }

    public function testCommand() {

        $config = new Config([
            'commands' => [
                'my_command' => 'Yab\Test\CommandTest\MyJob.index',
            ],
            'logger.default.display' => 1,
        ]);

        $file = rtrim(sys_get_temp_dir(), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.'test.tmp';

        $application = new Application();
        $application->configure($config);
        $application->launch(['script', 'my_command', 'p1', 'p2'], $file);

        $this->assertEquals(trim(file_get_contents($file)), 'success with p1 p2');
        
        unlink($file);

    }

    public function testRoute() {
       
        $test = $this;

        $config = new Config([
            'application.baseUrl' => '',
            'routes.put' => [
            '/api/v1/products' => function(Request $request, Response $response) use($test) {
                $test->assertTrue(true);
                return $response->setCode(201);
            }],
            'logger.default.display' => 0,
        ]);

        $request = new Request('PUT', '/api/v1/products');
  
        $application = new Application();
        $application->configure($config);
        $response = $application->route($request);

        $this->assertEquals($response->getCode(), 201);

    }

    public function testNotFound() {

        $config = new Config([
            'application.baseUrl' => '',
            'routes.put' => [
            '/api/v1/products' => function(Request $request, Response $response) {
                return $response->setCode(201);
            }],
            'logger.default.display' => 0,
        ]);

        $request = new Request('DELETE', '/api/v1/fake');
  
        $application = new Application();
        $application->configure($config);
        $response = $application->route($request);
  
        $this->assertEquals($response->getCode(), 404);

    }

}