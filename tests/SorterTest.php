<?php

namespace Yab\Test;

use Yab\Core\Sorter;
use Yab\Core\View;
use Yab\Core\Request;
use Yab\Core\Response;

use Yab\Test\ModelTest\Customer;

class SorterTest extends TestCase {
	
	protected $useDatabase = true;
    
    public function testSortAsc() {

        Customer::new(['first_name' => 'Charles'])->save();
        Customer::new(['first_name' => 'Yann'])->save();
        Customer::new(['first_name' => 'Albert'])->save();

        $customers = Customer::all();

        $request = new Request('GET', '/customers?s=first_name');
        $response = new Response(200);

        $sorter = new Sorter($request, $response);

        $sorter->sort($customers);

        $names = $customers->fetchValue('first_name')->toArray();
        
        $this->assertEquals($names, ['Albert', 'Charles', 'Yann']);
        $this->assertEquals($sorter->getSort(), 'first_name');
        $this->assertEquals($sorter->getSortField(), 'first_name');
        $this->assertEquals($sorter->getSortDir(), 'ASC');
        $this->assertEquals($sorter->getSortUrl('first_name'), '/customers?s=-first_name');
        $this->assertEquals($sorter->getSortUrl('last_name'), '/customers?s=last_name');

    }
    
    public function testSortDesc() {

        Customer::new(['first_name' => 'Charles'])->save();
        Customer::new(['first_name' => 'Yann'])->save();
        Customer::new(['first_name' => 'Albert'])->save();

        $customers = Customer::all();

        $request = new Request('GET', '/customers?s=-first_name');
        $response = new Response(200);

        $sorter = new Sorter($request, $response);

        $sorter->sort($customers);

        $names = $customers->fetchValue('first_name')->toArray();
        
        $this->assertEquals($names, ['Yann', 'Charles', 'Albert']);
        $this->assertEquals($sorter->getSort(), '-first_name');
        $this->assertEquals($sorter->getSortField(), 'first_name');
        $this->assertEquals($sorter->getSortDir(), 'DESC');
        $this->assertEquals($sorter->getSortUrl('first_name'), '/customers?s=first_name');
        $this->assertEquals($sorter->getSortUrl('last_name'), '/customers?s=last_name');

    }
    
    public function testSortLink() {

        $request = new Request('GET', '/customers?s=-first_name');
        $response = new Response(200);

        $sorter = new Sorter($request, $response);

        $link = $sorter->getSortLink('first_name', 'Prénom');

        $this->assertTrue($link instanceof View);
        $this->assertEquals($link->getTemplate(), 'templates/sorter-link.php');
        $this->assertEquals($link->sort, $sorter->getSort());
        $this->assertEquals($link->label, 'Prénom');

    }
    
    public function testCookie() {

        $request = new Request('GET', '/customers?cus=-first_name');
        $response = new Response(200);

        $sorter = new Sorter($request, $response, 'cu');

        $customers = Customer::all();

        $sorter->sort($customers);

        $this->assertEquals($sorter->getSort(), '-first_name');
        $this->assertEquals($response->getCookie('cus'), '-first_name');

    }
   
}