<?php

namespace Yab\Test;

use Yab\Core\Response;

class ResponseTest extends TestCase {
    
    public function testStaticCalls() {
        
        $this->assertEquals(200, Response::ok()->getCode());
        $this->assertEquals(404, Response::notFound()->getCode());

    }
    
    public function testFromString() {
        
        $responseString = 'HTTP/1.1 200 Ok'.PHP_EOL;
        $responseString .= 'Content-Type: text/html'.PHP_EOL;
        $responseString .= ''.PHP_EOL;
        $responseString .= '<p>Coucou</p>';

        $response = Response::fromString($responseString);

        $this->assertEquals($response->getProtocol(), 'HTTP/1.1');
        $this->assertEquals($response->getCode(), '200');
        $this->assertEquals($response->getHeader('Content-Type'), 'text/html');
        $this->assertEquals($response->getBody(), '<p>Coucou</p>');

    }
    
    public function testCookies() {

        $response = Response::ok();

        $cookies = $response->getCookies();

        $this->assertTrue(is_array($cookies));
        $this->assertTrue(empty($cookies));
        
        try {

            $response->getCookie('yann');

            $this->assertTrue(false);

        } catch(\Exception $e) {
            
            $this->assertTrue(true);

        }
        
        try {

            $response->getCookie('yann', '');

            $this->assertTrue(true);

        } catch(\Exception $e) {
            
            $this->assertTrue(false);

        }

        $response->setCookie('yann', 'truc', 120, '/toto');
        
        try {

            $cookie = $response->getCookieInfos('yann');

            $this->assertEquals($cookie, [
                'name' => 'yann',
                'value' => 'truc',
                'Expires' => 120,
                'Path' => '/toto',
            ]);

        } catch(\Exception $e) {
            
            $this->assertTrue(false);

        }

        $response->setCookie('test', 'valuetest');
        
        try {

            $cookie = $response->getCookieInfos('test');

            $this->assertEquals($cookie, [
                'name' => 'test',
                'value' => 'valuetest',
                'Expires' => 0,
                'Path' => '/',
            ]);

        } catch(\Exception $e) {
            
            $this->assertTrue(false);

        }

    }
   
}