<?php

namespace Yab\Test;

use Yab\Core\Request;

class RequestTest extends TestCase {
    
    public function testStaticCalls() {
        
        $this->assertEquals('GET', Request::get('/tests')->getVerb());
        $this->assertEquals('POST', Request::post('/tests')->getVerb());

    }
    
    public function testQueryString() {

        $request = new Request('GET', 'http://www.yab-core.com/');

        $this->assertEquals($request->getQueryString(), '');

        $this->assertTrue(is_array($request->getQueryParams()));

        $request->addQueryParam('test', 123);

        $this->assertEquals($request->getQueryParams(), ['test' => '123']);


    }
    
    public function testAnchor() {

        $request = new Request('GET', 'http://www.yab-core.com/');

        $request->setAnchor('anchor');

        $this->assertEquals('anchor', $request->getAnchor());

    }
    
    public function testSetQueryString() {

        $request = new Request('GET', 'http://www.yab-core.com/');

        $queryString = 'foo=bar&foo2=bar2';

        $params = [
            'foo' => 'bar',
            'foo2' => 'bar2',
        ];

        $request->setQueryString($queryString);

        $this->assertEquals($queryString, $request->getQueryString());

        $this->assertEquals($params, $request->getQueryParams());

        try {

            $request->getQueryParam('unknown');

            $this->assertTrue(false);

        } catch(\Exception $e) {

            $this->assertTrue(true);

        }

    }
    
    public function testAjax() {

        $request = new Request('GET', 'http://www.yab-core.com/');

        $this->assertFalse($request->isAjax());

        $request->setHeader('X-Requested-With', 'XmlHttpRequest');

        $this->assertTrue($request->isAjax());

    }
    
    public function testSession() {

        $_SESSION = [];

        $this->assertFalse(@Request::issetSession('unknown'));
        
        $this->assertEquals(Request::getSession('unknown', 123), 123);

        try {

            Request::getSession('unknown');

            $this->assertTrue(false);

        } catch(\Exception $e) {

            $this->assertTrue(true);

        }
        
        $this->assertTrue(is_array(Request::getSession()));

        Request::setSession('unknown', 123);

        $this->assertEquals(Request::getSession('unknown'), 123);

        $this->assertTrue(Request::issetSession('unknown'));

        Request::unsetSession('unknown');

        $this->assertFalse(Request::issetSession('unknown'));

        try {

            Request::unsetSession('unknown');

            $this->assertTrue(false);

        } catch(\Exception $e) {

            $this->assertTrue(true);

        }

        Request::setSession('unknown1', 123);
        Request::setSession('unknown2', 123);

        $this->assertEquals(2, count(Request::getSession()));

        $this->assertTrue(Request::clearSession());

        $this->assertEquals(0, count(Request::getSession()));
        
    }

    public function testMultiPart() {
        
        $boundary = '------WebKitFormBoundaryZaVLUKZFqnmSDPxs';

        $body = $boundary.PHP_EOL;
        $body.= 'Content-Disposition: form-data; name="bankId"'.PHP_EOL;
        $body.= ''.PHP_EOL;
        $body.= '1'.PHP_EOL;
        $body.= $boundary.PHP_EOL;
        $body.= 'Content-Disposition: form-data; name="operations_file"; filename=""'.PHP_EOL;
        $body.= 'Content-Type: application/octet-stream'.PHP_EOL;
        $body.= ''.PHP_EOL;
        $body.= ''.PHP_EOL;
        $body.= $boundary.PHP_EOL;
        $body.= 'Content-Disposition: form-data; name="submit"'.PHP_EOL;
        $body.= ''.PHP_EOL;
        $body.= 'Importer'.PHP_EOL;
        $body.= $boundary.PHP_EOL;

        $headers = [
            'Content-Type' => 'multipart/form-data; boundary='.$boundary,
        ];

        $request = new Request('POST', '/', $headers, $body);

        $bodyParams = [
            'bankId' => 1,
            'operations_file' => '',
            'submit' => "Importer"
        ];
        
        $this->assertTrue(strpos($request->getHeader('Content-Type'), 'multipart/form-data') === 0);
        $this->assertEquals($request->getBodyParams(), $bodyParams);
    }
    
    public function testCookies() {

        $request = new Request('GET', 'http://www.yab-core.com/');

        $cookies = $request->getCookies();

        $this->assertTrue(is_array($cookies));
        $this->assertTrue(empty($cookies));
        
        try {

            $request->getCookie('yann');

            $this->assertTrue(false);

        } catch(\Exception $e) {
            
            $this->assertTrue(true);

        }
        
        try {

            $request->getCookie('yann', '');

            $this->assertTrue(true);

        } catch(\Exception $e) {
            
            $this->assertTrue(false);

        }

        $request->setCookie('yann', 'truc');
        $request->setCookie('yann2', 'truc2');
        $request->setCookie('yann3', 'truc3');
        
        try {

            $cookie = $request->getCookie('yann');

            $this->assertEquals($cookie, 'truc');

        } catch(\Exception $e) {
            
            $this->assertTrue(false);

        }

        $cookie = $request->getHeader('Cookie');

        $this->assertEquals($cookie, 'yann=truc; yann2=truc2; yann3=truc3');

        $request->unsetCookie('yann2');

        $cookie = $request->getHeader('Cookie');

        $this->assertEquals($cookie, 'yann=truc; yann3=truc3');

        $cookies = $request->getCookies();

        $this->assertEquals($cookies , ['yann' => 'truc', 'yann3' => 'truc3']);

    }
   
    public function testSend() {

        $request = Request::get('https://www.google.fr');

        $response = $request->send();

        $this->assertEquals($response->getCode(), 200);

    }

}