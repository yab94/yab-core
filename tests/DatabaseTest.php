<?php

namespace Yab\Test;

use Yab\Core\Database;

class DatabaseTest extends TestCase {
	
	protected $useDatabase = true;
	
	public function testConnexion() {
		
		$this->createXCustomers(10);
		
		$stmt = Database::getPool()->statement('SELECT * FROM customer;');
		
		$stmt->execute();
		
		$records = $stmt->toArray();
		
		$this->assertTrue(0 < count($records));
		
	}

	public function testIntrospection() {
		
		$databases = Database::getPool()->showDatabases()->toArray();

		$this->assertTrue(is_array($databases));

		$this->assertTrue(in_array('main', $databases));

		$this->assertEquals('main', Database::getPool()->showDatabaseName());

		$tables = Database::getPool()->showTables()->toArray();

		$this->assertTrue(is_array($tables));
		$this->assertTrue(0 < count($tables));

		foreach($tables as $table) {

			$columns = Database::getPool()->tableColumns($table);

			$this->assertTrue(is_array($columns));
			$this->assertTrue(0 < count($columns));

			foreach($columns as $column) {

				$columnName =  Database::getPool()->columnName($column);

				$this->assertTrue(is_string($columnName));

				if(preg_match('#^(bill|customer|group)Id$#', $columnName)) {

					$this->assertTrue(Database::getPool()->columnIsPrimary($column));
					$this->assertTrue(Database::getPool()->columnIsSequence($column));

				} elseif(preg_match('#^through.+Id$#', $columnName)) {

					$this->assertTrue(Database::getPool()->columnIsPrimary($column));
					$this->assertFalse(Database::getPool()->columnIsSequence($column));

				} else {
					
					$this->assertFalse(Database::getPool()->columnIsPrimary($column));
					$this->assertFalse(Database::getPool()->columnIsSequence($column));

				}

			}

		}

	}
	
}