<?php

namespace Yab\Core;

class Application {

    use Tool;

    const EVENT_NO_ROUTE = 'Application::noRoute';
    const EVENT_PRE_ROUTE = 'Application::preRoute';
    const EVENT_POST_ROUTE = 'Application::postRoute';

    static protected $errors = [
        E_ERROR => 'E_ERROR', 
        E_WARNING => 'E_WARNING', 
        E_PARSE => 'E_PARSE', 
        E_NOTICE => 'E_NOTICE', 
        E_CORE_ERROR => 'E_CORE_ERROR',
        E_CORE_WARNING => 'E_CORE_WARNING', 
        E_COMPILE_ERROR => 'E_COMPILE_ERROR', 
        E_COMPILE_WARNING => 'E_COMPILE_WARNING', 
        E_USER_ERROR => 'E_USER_ERROR', 
        E_USER_WARNING => 'E_USER_WARNING', 
        E_USER_NOTICE => 'E_USER_NOTICE', 
        E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR', 
        E_DEPRECATED => 'E_DEPRECATED', 
        E_USER_DEPRECATED => 'E_USER_DEPRECATED', 
        E_ALL => 'E_ALL', 
    ];
    
    static protected $contextSetted = false;
    static protected $errorHandler = null;
    static protected $exceptionHandler = null;
    static protected $namespaces = [];

    public function __construct() {
        
        self::setContext();

        $this->configure(Config::fromIniFile(dirname(__DIR__).'/config/config.ini'));

    }

    public function __destruct() {

        self::unsetContext();

    }

    static public function setContext() {
        
        if(!defined('YAB_CORE_PATH'))
            define('YAB_CORE_PATH', dirname(__DIR__));

        if(self::$contextSetted)
            return false;

        self::$contextSetted = true;

        spl_autoload_register([__CLASS__, 'loadClass']);
        
        self::$errorHandler = set_error_handler([__CLASS__, 'errorHandler']);
        self::$exceptionHandler = set_exception_handler([__CLASS__, 'exceptionHandler']);

        register_shutdown_function([__CLASS__, 'shutdownFunction']);

        return true;
        
    }

    static public function unsetContext() {
 
        if(!self::$contextSetted)
            return false;

        self::$contextSetted = false;

        set_error_handler(self::$errorHandler);
        set_exception_handler(self::$exceptionHandler);
        
        return true;

    }

    static public function addNamespace($namespace, $directory) {

        if(!isset(self::$namespaces[$namespace]))
            self::$namespaces[$namespace] = [];
        
        self::$namespaces[$namespace][$directory] = $directory;

    }

    static private function loadClass($class) {

        foreach (self::$namespaces as $namespace => $autoloadDirectories) {
          
            $namespace = ltrim($namespace, '\\').'\\';
        
            if(strpos($class, $namespace) !== 0)
                continue;
            
            $classPath = substr(str_replace('\\', DIRECTORY_SEPARATOR, $class), strlen($namespace)).".php";
        
            foreach($autoloadDirectories as $autoloadDirectory) {
        
                $filePath = $autoloadDirectory . DIRECTORY_SEPARATOR . $classPath;
                
                if (!file_exists($filePath)) {
                    
                    continue;
                }
                
                include_once $filePath;
                
                if (!class_exists($class, false) && !interface_exists($class) && !trait_exists($class, false)) {
                    throw new \Exception('beware of the file "' . $filePath . '" that dont define class "' . $class . '"');
                }

                return true;
                
            }    

        }

    }

    static public function errorHandler($errno, $errstr, $errfile, $errline) {

        $error = isset($errno, self::$errors) ? self::$errors[$errno] : 'UNKNOWN('.$errno.')';

        $message = $error . ': ' . $errstr . ' (' . $errfile . ':L' . $errline . ')';
        
        if(preg_match('#(NOTICE)#', $error)) {

            Logger::notice($message);

        } elseif(preg_match('#(WARNING)#', $error)) {

            Logger::warning($message);

        } elseif(preg_match('#(ERROR|ALL)#', $error)) {

            Logger::error($message);

        } elseif(preg_match('#(DEPRECATED)#', $error)) {

            Logger::alert($message);

        } else {

            Logger::debug($message);

        }

        return null;

    }

    static public function exceptionHandler(\Throwable $e) {

        Logger::error("Type: " . get_class($e) . "; Message: {$e->getMessage()}; File: {$e->getFile()}; Line: {$e->getLine()};");
        
        return null;

    }

    static public function shutdownFunction() {

        $error = error_get_last();
        
        if(!is_array($error))
            return;

        if (in_array($error['type'], [E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR, E_RECOVERABLE_ERROR, E_CORE_WARNING, E_COMPILE_WARNING, E_PARSE])) {
            return self::errorHandler($error["type"], $error["message"], $error["file"], $error["line"]);
        } 
    }

    public function configure(Config $config): Application {

        // Paramétrage de PHP
        foreach(Tool::flattenArray($config->getParam('php', [])) as $param => $value) 
            ini_set($param, $value);
        
        // Paramétrage de l'Autoload
        foreach($config->getParam('autoload', []) as $namespace => $directory) 
            self::$namespaces[(string) $namespace][] = (string) $directory;
            
        // Paramétrage de l'include path
        foreach($config->getParam('include.directories', []) as $directory)
            set_include_path($directory.PATH_SEPARATOR.get_include_path());

        // Paramétrage des loggers
        foreach($config->getParam('logger', []) as $name => $params) {
            Logger::setPool(new Logger(), $name);
            foreach($params as $key => $value) {
                $setter = 'set'.ucfirst($key);
                Logger::getPool($name)->$setter($value);
            }
        }
        
        // Paramétrage des base de données
        foreach($config->getParam('database', []) as $name => $params) {
            Database::setPool(new Database(
                $config->getParam('database.' . $name . '.dsn'), 
                $config->getParam('database.' . $name . '.username'), 
                $config->getParam('database.' . $name . '.password'), 
                $config->getParam('database.' . $name . '.driver_options', [])
            ), $name);
        }
        
        // Paramétrage des évenements
        foreach($config->getParam('event', []) as $event => $listener) 
            Event::addListeners($event, $listener);
            
        Config::getGlobal()->fusion($config);

        // Prise en compte de fichiers de configuration additionnels
        foreach ($config->getParam('application.additionnal_config_files', []) as $additional_config_file) 
            $this->configure(Config::fromIniFile($additional_config_file));

        return $this;

    }

    public function launch(array $argv = [], $stdout = 'php://output') {

        try {

            if(!count($argv))
                $argv = $_SERVER['argv'];

            $executed = false;

            $currentScript = array_shift($argv);
            $askedCommand = array_shift($argv);
            $params = $argv;

            foreach(Config::get('commands') as $command => $closure) {

                if (!$askedCommand || ($command != $askedCommand))
                    continue;
    
                $executed = true;
 
                if($closure instanceof \Closure) {
                    $closure($stdout, ...$params);
                    break;
                }
    
                $parts = preg_split('#\s+#', $closure);
                $commandAction = array_shift($parts);
                $commandAction = explode('.', $commandAction);
                $command = array_shift($commandAction);
                $action = array_shift($commandAction);
        
                if (!class_exists($command)) {
                    throw new \Exception('unexisting class "' . $command . '"');
                }
        
                if (!is_subclass_of($command, Command::class, true)) {
                    throw new \Exception('bad command class "' . $command . '"');
                }
    
                $command = new $command($stdout);
                $command->before();
                $command->$action(...$params);
                $command->after();

                break;

            }

            if(!$executed) {

                $usage = 'Usage: '.implode(' ', $argv).' [COMMAND]'.PHP_EOL;
                $usage .= "List of available commands:".PHP_EOL;
        
                foreach(Config::get('commands') as $command => $closure)
                    $usage .= " - ".$command.PHP_EOL; 

                throw new \Exception(trim($usage));

            }

        } catch(\Throwable  $e) {
            
            file_put_contents($stdout, $e->getMessage());

            $this->exceptionHandler($e);

        }

	}

    public function route(Request $request = null, Response $response = null): Response {

        if($request === null)
            $request = Request::fromGlobals();

        if($response === null)
            $response = new Response(404);

        try {

            Event::fire(self::EVENT_PRE_ROUTE, $request, $response);
      
            $response->setCode(404);

            foreach(Config::get('routes') as $verb => $routes) {

                if(strtolower($verb) != 'all' && (strtolower($verb) != strtolower($request->getVerb())))
                    continue;

                $routed = false;
                
                foreach($routes as $route => $closure) {
                    
                    $regexp = '#^'.Config::get('application.baseUrl').$route.'$#';
            
                    preg_match_all('#:([a-zA-Z0-9_]+)#', $regexp, $matchKeys);
            
                    $matchKeys = $matchKeys[1];
        
                    foreach($matchKeys as $var)
                        $regexp = str_replace(':'.$var, '([^/]+)', $regexp);
            
                    Logger::debug('route try "'.$regexp.' =~ '.$request->getPath().'"');
            
                    if(!preg_match($regexp, $request->getPath(), $matchValues))
                        continue;
                        
                    $routed = true;

                    $response->setCode(200);
            
                    Logger::debug('route match "'.$verb.':'.$route.'"');

                    $params = [];
            
                    array_shift($matchValues);
            
                    foreach(array_combine($matchKeys, $matchValues) as $key => $value)
                        $params[$key] = $value;
        
                    if($closure instanceof \Closure) {
                        $closure($request, $response);
                        break;
                    }
            
                    $parts = preg_split('#\s+#', $closure);
                    $controllerAction = array_shift($parts);
                    $controllerAction = explode('.', $controllerAction);
                    $controller = array_shift($controllerAction);
                    $action = array_shift($controllerAction);
                    $params = array_intersect_key($params, array_flip($parts));
            
                    if (!class_exists($controller)) 
                        throw new \Exception('unexisting class "' . $controller . '"');
                    
                    if (!is_subclass_of($controller, Controller::class, true)) 
                        throw new \Exception('bad controller class "' . $controller . '"');
    
                    $params = array_values($params);

                    $controller = new $controller($request, $response);
                    $controller->before();
                    $controller->$action(...$params);
                    $controller->after();
    
                    break;

                }

                if($routed) break;
    
            }

            if($response->getCode() == 404) {

                Event::fire(self::EVENT_NO_ROUTE, $request, $response);

                throw new \OutOfBoundsException('unable to route request', 404);

            }

            Event::fire(self::EVENT_POST_ROUTE, $request, $response);

        } catch(\Throwable $e) {

            try {

                $response->setCode($e->getCode());

            } catch(\Exception $ec) {

                $response->setCode(500);

            }
                
            $this->exceptionHandler($e);

            $traces = $e->getTrace();
        
            $trace = [
                'file' => $e->getFile(), 
                'line' => $e->getLine(), 
                'class' => '', 
                'function' => '', 
                'args' => []
            ];
            
            array_unshift($traces, $trace);
        
            $view = new View();
    
            if($response->getCode(404)) {
                
                $view->setTemplate('templates/errors/not-found.php');
    
            } else {
    
                $view->setTemplate('templates/errors/server-error.php');
    
            }
            
            $view->exception = $e;
            $view->traces = $traces;
    
            $response->appendBody($view->getRender());

        } finally {
            
            if (in_array($_SERVER['REMOTE_ADDR'] ?? 'none', preg_split('#\s+#s', trim(Config::get('debug.clients', ''))))) 
                $response->appendBody($this->debugBar($request, $response, $e ?? null));

        }

        return $response;

    }
    
    public function debugBar(Request $request, Response $response, \Throwable $e = null): View {
    
        $view = new View();
        
        $view->setTemplate('templates/debug.php');
        
        $view->request = $request;
        $view->response = $response;
        
        if($e !== null) {

            $traces = $e->getTrace();
            
            $trace = [
                'file' => $e->getFile(), 
                'line' => $e->getLine(), 
                'class' => '', 
                'function' => '', 
                'args' => []
            ];
            
            array_unshift($traces, $trace);

            $view->traces = $traces;
            $view->title = 'A '.get_class($e).' has been caught!';
            $view->message = $e->getMessage();

        } else {
            
            $view->title = '[YDB] - Yab Debug Bar';
            $view->message = '';

            $view->traces = [];

        }
        
        return $view;
        
    }

}