<?php

namespace Yab\Core;

class Event {

    static protected $listeners = [];

    static public function fire(string $event, ...$params): void {

        $event = strtoupper($event);

        Logger::debug('firing event "'.$event.'"');
        
        if(!isset(self::$listeners[$event]))
            return;

        foreach(self::$listeners[$event] as $listenerId => $closure) {

            Logger::debug('triggering listener "'.$event.':'.$listenerId.'"');

            $closure(...$params);

        }
        
    }

    static public function getEventListeners() {

        return self::$listeners;

    }

    static public function getListeners(string $event) {

        return self::$listeners[strtoupper($event)] ?? [];

    }

    static public function getListener(string $event, string $listenerId) {

        $event = strtoupper($event);

        if(!isset(self::$listeners[$event]))
            throw new \Exception('unknown event "'.$event.'"');

        if(!isset(self::$listeners[$event][$listenerId]))
            throw new \Exception('unknown event listener "'.$listenerId.'"');

        return self::$listeners[$event][$listenerId];

    }
    
    static public function removeAllListeners(): void {

        self::$listeners = [];
        
    }
    
    static public function removeListeners(string $event): void {

        self::$listeners[strtoupper($event)] = [];
        
    }
    
    static public function removeListener(string $event, string $listenerId): void {

        $event = strtoupper($event);

        if(!isset(self::$listeners[$event]))
            throw new \Exception('unknown event "'.$event.'"');

        if(!isset(self::$listeners[$event][$listenerId]))
            throw new \Exception('unknown event listener "'.$listenerId.'"');

        unset(self::$listeners[$event][$listenerId]);
        
    }
    
    static public function addListeners(string $event, array $listeners): void {

        foreach($listeners as $listenerId => $closure)
            self::addListener($event, $closure, $listenerId);
        
    }
    
    static public function addListener(string $event, $closure, string $listenerId = null): string {

        $event = strtoupper($event);

        if(!isset(self::$listeners[$event]))
        self::$listeners[$event] = [];

        if(!($closure instanceof \Closure))
            $closure = Tool::toClosure($closure);

        if($listenerId === null)
            $listenerId = $event.'_'.(count(self::$listeners[$event]) + 1);

        self::$listeners[$event][$listenerId] = $closure;

        return $listenerId;
        
    }

}