<?php

namespace Yab\Core;

class Response {

    use Http, Tool;

    protected $code = null;

    public function __construct(int $code, string $body = '') {

        $this->setCode($code);

        if($body)
            $this->setBody($body);

    }

    static public function fromString(string $string, $jumpContinue = true): Response {

        $parts = explode("\r\n\r\n", $string);

        $headers = array_shift($parts);
        $body = implode("\r\n\r\n", $parts);

        $headers = explode("\r\n", $headers);

        $firstHeader = array_shift($headers);

		$response = self::fromHttpHeader($firstHeader);

        if($jumpContinue && $response->getCode() == 100)
            return self::fromString($body);

        foreach($headers as $header) {

            $header = explode(':', $header);

            $headerName = array_shift($header);
            $headerValue = implode(':', $header);

            $response->setHeader($headerName, $headerValue);

        }

		$response->body = (string) $body;
		
		return $response;
		
    }

    static public function fromHttpHeader(string $header): Response {

        if(!preg_match('#(http/[0-9\.]+)\s+([0-9]+)\s*.*$#i', $header, $match))
            throw new \Exception('unable to generate response from header "'.$header.'"');

        $response = new self($match[2]);

        $response->setProtocol($match[1]);

        return $response;

    }

    static public function __callStatic($method, array $args): Response {

        return new self(self::codeByMessage($method), ...$args);

    }

    public function setCode(int $code): Response {

        if(!isset(self::$codes[$code]))
            throw new \Exception('invalid HTTP code "'.$code.'"');

        $this->code = (int) $code;

        return $this;
    }

    public function getCode(): int {

        return $this->code;
        
    }

    public function redirect($url, $code = 303) {

        $this->code = $code;

        return $this->setHeader('Location',  $url);

    }

    public function flush() {

        if(!headers_sent()) {

            header($this->protocol." ".$this->code." ".self::$codes[$this->code]);

            foreach($this->headers as $headerName => $headerValues) {

                foreach($headerValues as $headerValue) {
                    
                    header(ucwords($headerName, '-').': '.$headerValue, false);

                }

            }

        }

        while(0 < ob_get_level())
            $this->body .= ob_get_clean();
        
        echo $this->body;

        $this->body = '';

    }

    public function send() {

        $this->flush();

        exit();

    }
    
    public function setCookie($name, $value, $expires = 0, $path = '/') {

        return $this->addHeader('Set-Cookie', $name.'='.urlencode($value).'; Expires='.$expires.'; Path='.$path);

    }

    public function getCookies(): array {

        try {

            $cookies = [];

            foreach($this->getHeaders('Set-Cookie') as $header) {

                $parts = explode(';', $header);

                $subParts = explode('=', trim(array_shift($parts)));
                
                $cookie = [
                    'name' => array_shift($subParts),
                    'value' => urldecode(array_shift($subParts)),
                ];

                foreach($parts as $part) {

                    $subParts = explode('=', trim($part));
                    
                    $cookie[array_shift($subParts)] = urldecode(array_shift($subParts));

                }

                $cookies[$cookie['name']] = $cookie;

            }

            return $cookies;

        } catch(\Exception $e) {

            return [];

        }

    }

    public function getCookieInfos(string $name): array {

        $cookies = $this->getCookies();

        if(!isset($cookies[$name]))
            throw new \Exception('invalid cookie name "'.$name.'"');
        
        return $cookies[$name];

    }

    public function getCookie($name, $defaultValue = null) {

        $cookies = $this->getCookies();

        if(!isset($cookies[$name]) && $defaultValue === null)
            throw new \Exception('invalid cookie name "'.$name.'"');
        
        return isset($cookies[$name]) ? $cookies[$name]['value'] : $defaultValue;

    }

}