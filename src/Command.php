<?php

namespace Yab\Core;
        
abstract class Command {

    use Tool;

    static private $styles = [
		'bold'		=>  '1',
		'italic'	=>  '3',
		'blink'	    =>  '5',
		'underline'	=>  '4',
		'green'		=>  '0;32',
		'red'		=>  '0;31',
		'yellow'	=>  '1;33',
		'blue'		=>  '0;34',
		'cyan'		=>  '0;36',
		'purple'	=>  '0;35',
		'black'		=>  '0;30',
		'white'		=>  '1;37',
		'gray'		=>  '1;30',
		'bg-green'	=>  '42',
		'bg-red'	=>  '41',
		'bg-yellow'	=>  '43',
		'bg-blue'   =>  '44',
		'bg-cyan'	=>  '46',
		'bg-magenta'=>  '45',
		'bg-black'	=>  '40'
	];

    protected $output = null;

    final public function __construct($output) {

        $this->output = (string) $output;

    }

    public function getCommand() {

        return $this->command;

    }

    public function getParam($index, $default = null) {

        return isset($this->params[$index]) ? $this->params[$index] : $default;

    }

    public function getParams() {

        return $this->params;

    }

    public function read($hide = false) {
    
        if(!$hide) 
            return rtrim(fgetc(STDIN), PHP_EOL);

        $oldStyle = shell_exec('stty -g');
        
        shell_exec('stty -icanon -echo min 1 time 0');

        $param = '';
        
        while (true) {
            
            $char = fgetc(STDIN);

            if ($char === "\n") {
                break;
            } else if (ord($char) === 127) {
                if (strlen($param) > 0) {
                    fwrite(STDOUT, "\x08 \x08");
                    $param = substr($param, 0, -1);
                }
            } else {
                fwrite(STDOUT, "*");
                $param .= $char;
            }
        }
        
        shell_exec('stty ' . $oldStyle);
        
        return rtrim($param, PHP_EOL);
        
    }

    protected function write($message, $newLine = true) {

        if($newLine)
            $message .= PHP_EOL;
    
        $handler = fopen($this->output, 'w');
        
        if(!$handler)
            throw new \Exception('unable to open php std output');
        
        $written = fwrite($handler, $message);
        
        if($written < strlen($message))
            throw new \Exception('unable to fully write into std output');
        
        fclose($handler);
        
        return true;
        
    }

    protected function startDaemon($name, $startClosure, $stopClosure) {

        $pidFile = Config::get('general.pidDir', '/var/run') . DIRECTORY_SEPARATOR . $name . Config::get('general.pidExt', '.pid');

        if (file_exists($pidFile))
            throw new \Exception('can not start daemon because it is already started (' . $pidFile . ':' . file_get_contents($pidFile) . ')');

        $pid = pcntl_fork();

        if ($pid == -1) {

            throw new \Exception('fork error');

        } elseif ($pid) {

            return Logger::info('daemon started');
        }

        posix_setsid();

        $pid = posix_getpid();

        file_put_contents($pidFile, $pid);

        declare(ticks = 1);

        pcntl_signal(SIGTERM, function($sig) use($pidFile, $stopClosure) {

            $this->runClosure($stopClosure);

            if (file_exists($pidFile) && !unlink($pidFile)) {
                Logger::warning('failed to delete pid file "' . $pidFile . '"');
            }

            Logger::info('daemon stopped');

            exit();
        });

        chdir('/');

        umask(0);

        return $this->runClosure($startClosure);
    }

    protected function stopDaemon($name) {

        $pidFile = Config::get('general.pidDir', '/var/run') . DIRECTORY_SEPARATOR . $name . Config::get('general.pidExt', '.pid');

        if (!file_exists($pidFile))
            throw new \Exception('can not stop daemon because it is not started');

        $pid = file_get_contents($pidFile);

        if (!posix_kill($pid, SIGTERM))
            Logger::warning('failed to stop process "' . $pid . '"');

        if (file_exists($pidFile) && !unlink($pidFile))
            Logger::warning('failed to delete pid file "' . $pidFile . '"');

        return true;
    }
    
	protected function decorate($message, array $options) {
        
        $decorations = '';
            
        foreach($options as $option) {

            if(array_key_exists($option, self::$styles)) 
                $decorations .= "\033[" . self::$styles[$option] . "m";
            
                
        }
        
		return $decorations.$message."\033[0m";
        
	}
    
    protected function askPassword($question) {

        $message = $question.' ? ';
        
        $this->write($message, false);

        $param = $this->input->read(true);
        
        $this->write(PHP_EOL);
        
        return $param;
        
    }
    
    protected function askParam($question, $defaultParam = null, $retry = 0) {
        
        $message = $question.' ? ';
        
        if($defaultParam)
            $message .= '(default: '.$defaultParam.') ';
        
        $this->write($message, false);

        $param = $this->read();

        if($param) 
            return $param;

        if($defaultParam !== null)
            return $defaultParam;
        
        return $this->askParam($question, $defaultParam, $retry++);
        
    }

    public function before() {}

    public function after() {}

}