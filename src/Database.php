<?php

namespace Yab\Core;

class Database extends \PDO {
	
	const EVENT_QUERY = 'Database::onQuery';
	const DEFAULT_DATABASE = 'default';

	static protected $pool = [];

	static public function getPool(string $name = self::DEFAULT_DATABASE): Database {

		if(!isset(self::$pool[$name]))
			throw new \Exception('can not find any database "'.$name.'" in the database pool');

		return self::$pool[$name];

	}

	static public function setPool(Database $database, string $name = self::DEFAULT_DATABASE): void {

		self::$pool[$name] = $database;

	}

    public function quote($string, $paramtype = null): string {
		
		if($string === null)
			return 'NULL';
			
        return parent::quote($string, $paramtype);

    }

    public function query(string $sql) {
        
        Event::fire(self::EVENT_QUERY, $sql);
        
        $result = parent::query($sql);
		
        if(!$result)
            throw new \Exception('SQL Error: '.implode(' ', $this->errorInfo()));

		return $result;

    }
    
    final public function tablePrimary(string $table): array {

		$primary = [];
	
        foreach($this->tableColumns($table) as $column) {

            if($this->columnIsPrimary($column))
                array_push($primary, $this->columnName($column));
            
        }

        return $primary;

    }
		
    final public function tableColumns(string $table): array {
		
		$driver = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
		
		switch($driver) {
			
			case 'mysql':
				return $this->statement('DESC '.$this->identifier($table))->toArray();
		
			case 'sqlite':

				$columns = $this->statement('PRAGMA table_info('.$this->identifier($table).');')->toArray();

				$fields = $this->statement('SELECT sql FROM sqlite_master WHERE type=\'table\' AND name='.$this->quote($table).';')->fetchValue('sql')->next();
		
				foreach($columns as &$column) 
					$column['autoincrement'] = preg_match('#'.$column['name'].'[^,]+AUTOINCREMENT#i', $fields) ? true : false;

				return $columns;
			
		}
		
		throw new \Exception('columnIsSequence not implemented for driver "'.$driver.'"');

    }
		
    final public function tableColumnNames(string $table): array {
		
		$driver = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
		
		switch($driver) {
			
			case 'mysql':
				return $this->tableColumns($table)->fetchValue('Field');
	
			case 'sqlite':
				return $this->tableColumns($table)->fetchValue('Field');
			
		}
		
		throw new \Exception('columnIsSequence not implemented for driver "'.$driver.'"');
        
    }
		
    final public function columnIsPrimary(array $column): bool {
		
		$driver = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
		
		switch($driver) {
			
			case 'mysql':
				return in_array($column['Key'], ['PRI', 'MUL']);

			case 'sqlite':
				return 0 < $column['pk'];
			
		}
		
		throw new \Exception('columnIsSequence not implemented for driver "'.$driver.'"');
        
    }
		
    final public function columnName(array $column): string {
		
		$driver = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
		
		switch($driver) {
			
			case 'mysql':
				return $column['Field'];

			case 'sqlite':
				return $column['name'];
		
		}
		
		throw new \Exception('columnName not implemented for driver "'.$driver.'"');
        
    }
		
    final public function columnIsSequence(array $column): bool {
		
		$driver = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
		
		switch($driver) {
			
			case 'mysql':
				return $column['Extra'] == 'auto_increment';
		
			case 'sqlite':
				return $column['autoincrement'] == true;
			
		}
		
		throw new \Exception('columnIsSequence not implemented for driver "'.$driver.'"');

    }
	
	final public function showDatabaseName(): string {
		
		$driver = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
		
		switch($driver) {
			
			case 'mysql':
				return $this->statement('SELECT DATABASE();')->fetchValue(0)->next();
		
			case 'sqlite':
				return $this->showDatabases()->next();
			
		}
		
		throw new \Exception('showDatabaseName not implemented for driver "'.$driver.'"');
		
		
		
	}
	
	final public function showDatabases(): Statement {
		
		$driver = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
		
		switch($driver) {
			
			case 'mysql':
				return $this->statement('SHOW DATABASES;')->fetchValue('Database');
		
			case 'sqlite':
				return $this->statement('PRAGMA database_list;')->fetchValue('name');
			
		}
		
		throw new \Exception('showDatabases not implemented for driver "'.$driver.'"');
		
	}
    
	final public function showTables(): Statement {
		
		$driver = $this->getAttribute(\PDO::ATTR_DRIVER_NAME);
		
		switch($driver) {
			
			case 'mysql':
				return $this->statement('SHOW TABLES;')->fetchValue(0);
		
			case 'sqlite':
				return $this->statement('SELECT * FROM sqlite_master WHERE type=\'table\';')->fetchValue('tbl_name');
			
		}
		
		throw new \Exception('showTables not implemented for driver "'.$driver.'"');
		
	}
	
    final public function select(string $table): Statement {

        return $this->statement('SELECT * FROM ' . $this->identifier($table));
    }

    final public function update(string $table): Statement {

        return $this->statement('UPDATE ' . $this->identifier($table) . ' SET ');
    }

    final public function insert(string $table): Statement {

        return $this->statement('INSERT INTO ' . $this->identifier($table));
    }

    final public function delete(string $table): Statement {

        return $this->statement('DELETE FROM ' . $this->identifier($table));
    }

    final public function statement(string $sql): Statement {

        return new Statement($this, $sql);
    }

    final public function identifier(string $value): string {

        $values = is_array($value) ? $value : array($value);

        foreach ($values as $key => $value) {

            $prefix = '';
            $alias = '';

            $parts = explode('.', (string) $value);

            if (1 < count($parts))
                $prefix = array_shift($parts) . '.';

            $field = implode('.', $parts);

            $parts = preg_split('#\s+#', $field);

            if (1 < count($parts))
                $alias = ' ' . array_shift($parts);

            $field = implode(' ', $parts);

            $values[$key] = $prefix . ($field == '*' ? '*' : '`' . $field . '`') . $alias;
        }

        return implode(', ', $values);
    }

}

// Do not clause PHP tags unless it is really necessary
