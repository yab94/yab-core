<?php

namespace Yab\Core;

class Field extends View {
    
    protected $name = '';
    protected $value = null;
    
    protected $options = [];
    protected $multiple = [];

    protected $validable = true;
    protected $valuable = true;

    protected $rules = [];

    protected $onRequest = null;
    protected $onView = null;

    protected $errors = [];
   
    public function __construct(string $name) {

        $this->name = $name;
        
    }

    public function getName() {

        return $this->name.($this->multiple ? '[]' : '');
        
    }

    public function getValue() {

        return $this->value;
        
    }

    public function setOptions(array $options): Field {

        $this->options = $options;

        return $this;
        
    }

    public function setMultiple(bool $multiple): Field {

        $this->multiple = (bool) $multiple;

        return $this;
        
    }

    public function isMultiple(): bool {

        return $this->multiple;
        
    }

    public function setValue($value): Field {

        $this->value = $value;

        return $this;
        
    }

    public function onRender(): View {

        $onView = $this->onView ?? function($value) { return $value; };

        return $this;

    }

    public function onRequest(\Closure $onRequest = null) {

        $this->onRequest = $onRequest;

        return $this;

    }

    public function onView(\Closure $onView = null) {

        $this->onView = $onView;

        return $this;

    }

    public function setValidable(bool $validable): Field {

        $this->validable = (bool) $validable;

        return $this;

    }

    public function isValidable(): bool {

        return $this->validable;

    }

    public function setValuable(bool $valuable): Field {

        $this->valuable = (bool) $valuable;

        return $this;

    }

    public function isValuable(): bool {

        return $this->valuable;

    }

    public function getErrors(): array {

        return $this->errors;

    }

    public function isRequired(): bool {

        return ! (clone $this)->process('');

    }

    public function process($requestValue): bool {
     
        $validation = true;

        $onRequest = $this->onRequest ?? function($value) { return $value; };

        $value = $onRequest($requestValue);

        # important for checkboxes
        if($value !== null) 
            $this->setValue($value);
        
        $this->errors = [];
       
        foreach($this->rules as $index => $closure) {

            try {

                $closure($this->getValue());
                
            } catch(\Exception $e) {

                $this->errors[] = $e->getMessage();

                $validation = false;

            }
            
        }

        return $validation;
    }

    final public function addMatchingRule($regexp, $error): Field {

        return $this->addRule(function($value) use ($regexp, $error) { if(!preg_match($regexp, $value)) throw new \Exception($error); });
    
	}

    final public function addOptionsRule(array $options, $error = 'bad option value'): Field {

        return $this->setOptions($options)->addRule(function($value) use ($options, $error) { if(!isset($options[$value])) throw new \Exception($error); });
    
	}

    final public function addRequiredRule($error): Field {

        return $this->addRule(function($value) use ($error) { if($value == '') throw new \Exception($error); });
    
    }
    
    public function addUploadRule($destination, $mimes = [], $size = 0): Field {

        return $this->addRule(function($value) use ($destination, $mimes, $size) {

            if(!is_dir($destination) || !is_writable($destination))
                throw new \Exception('invalid destination folder for upload');

            if(preg_match('#[\x00-\x1F\x7F-\x9F/\\\\]#', $value['name']))
                throw new \Exception('bad file name invalid character');

            if(!is_file($value['tmp_name']) || !is_uploaded_file($value['tmp_name']))
                throw new \Exception('can not upload file, no tmp file found');

            if(count($mimes) && !in_array($value['type'], $mimes))
                throw new \Exception('can not upload file, invalid MIME type');

            if($size && $size < $value['size'])
                throw new \Exception('can not upload file, file too big');

            $destination = $destination.DIRECTORY_SEPARATOR.$value['name'];

            if(!move_uploaded_file($value['tmp_name'], $destination))
                throw new \Exception('failed to upload file');
            
        });

    }

    final public function removeRules(): Field {

        $this->rules = [];

        return $this;
    }

    final public function addRule(\Closure $closure): Field {

        $this->rules[] = $closure;

        return $this;
    }

}

// Do not clause PHP tags unless it is really necessary