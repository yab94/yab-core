<?php

namespace Yab\Core;

class Config {

    static protected $global = null;

	protected $params = [];

    static public function getGlobal(): Config {

        if(self::$global === null)
            self::$global = new Config();

        return self::$global;
        
    }

    static public function get(string $param, $default = null) {

        return self::getGlobal()->getParam($param, $default);
        
    }

    static public function set(string $param, $value): Config {

        return self::getGlobal()->setParam($param, $value);
        
    }

    static public function fromIniFile(string $iniFile): Config {

        if (!file_exists($iniFile)) {
            throw new \Exception('unable to find config file "' . $iniFile . '"');
        } 

        return new Config(parse_ini_file($iniFile, true));
        
    }
    
	public function __construct(array $params = []) {
		
        $this->params = $this->digConfigDotParams($params);
		
	}

    public function getParams(): array {
        
        return $this->params;
        
    }

    public function getParam(string $param, $default = null) {

        $parts = explode('.', $param);

        $part = array_shift($parts);

        if (!isset($this->params[$part])) {

            if ($default === null) {
                throw new \Exception('unable to find config param "' . $param . '" "'.$part.'"');
            }

            return $default;
        }

        $param = $this->params[$part];

        while (count($parts)) {

            $part = array_shift($parts);

            if (!isset($param[$part])) {

                if ($default === null) {
                    throw new \Exception('unable to find config param "'.$part.'"');
                }

                return $default;
            }

            $param = $param[$part];
        }

        return $param;
    }
    
	public function setParam(string $name, $value): Config {

        $name = explode('.', $name);

        $section = array_shift($name);

        $name = implode('.', $name);

        return $this->fusion(new Config($name ? [$section => [$name => $value]] : [$section => $value]));
		
    }
    
	public function fusion(Config $config): Config {

        $this->params = $this->mergeArrays($this->params, $config->params);

		return $this;
		
    }
 
    protected function mergeArrays(array $arrayA, array $arrayB): array {

        foreach($arrayB as $keyB => $valueB) {

            if(is_numeric($keyB)) {

                if(is_array($valueB)) {

                    $arrayA = $this->mergeArrays($arrayA, $valueB);

                } elseif(!in_array($valueB, $arrayA)) {

                    array_push($arrayA, $valueB);

                }

            } elseif(!isset($arrayA[$keyB])) {

                $arrayA[$keyB] = $valueB;

            } elseif(is_array($arrayA[$keyB]) && is_array($valueB)) {

                $arrayA[$keyB] = $this->mergeArrays($arrayA[$keyB], $valueB);

            } elseif(!is_array($valueB)) {

                $arrayA[$keyB] = $valueB;

            }

        }

        return $arrayA;

    }
	
    protected function digConfigDotParams(array $config): array {

        foreach ($config as $key => $value) {

            unset($config[$key]);

            $keyParts = explode('.', $key);

            $depth = &$config;

            foreach ($keyParts as $keyPart) {

                if (defined($keyPart)) {
                    $keyPart = constant($keyPart);
                }

                $depth[$keyPart] = isset($depth[$keyPart]) ? $depth[$keyPart] : [];

                $depth = &$depth[$keyPart];
            }

            if (is_array($value)) {

                $depth = $this->digConfigDotParams($value);
            } else {

                $depth = $value;
            }
        }

        return $config;
    }
    
}
