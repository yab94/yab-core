<?php

namespace Yab\Core;

if(!defined('LOG_LOCAL0'))
    define('LOG_LOCAL0', 0);

class Logger {

	const DEFAULT_LOGGER = 'default';

    const LOG_EMERG = 0;
    const LOG_ALERT = 1;
    const LOG_CRIT = 2;
    const LOG_ERR = 3;
    const LOG_WARNING = 4;
    const LOG_NOTICE = 5;
    const LOG_INFO = 6;
    const LOG_DEBUG = 7;

    static protected $priorities = [
        self::LOG_EMERG => 'LOG_EMERG', // 	système inutilisable
        self::LOG_ALERT => 'LOG_ALERT', // 	une décision doit être prise immédiatement
        self::LOG_CRIT => 'LOG_CRIT', // 	condition critique
        self::LOG_ERR => 'LOG_ERR', // 	condition d'erreur
        self::LOG_WARNING => 'LOG_WARNING', // 	condition d'alerte
        self::LOG_NOTICE => 'LOG_NOTICE', // 	condition normale, mais significative
        self::LOG_INFO => 'LOG_INFO', // 	message d'information
        self::LOG_DEBUG => 'LOG_DEBUG', // 	message de déboguage
    ];

    static protected $debugMessages = [];
	static protected $pool = [];

    protected $priority = self::LOG_ERR;
    protected $ident = '';
    protected $prefix = '';
    protected $display = false;
    protected $options = LOG_CONS | LOG_ODELAY | LOG_PID;
    protected $facility = LOG_LOCAL0;
    
    static public function getDebugMessages() {
        
        return self::$debugMessages;
        
    }
    
    public function setIdent($ident): Logger {

        $this->ident = (string) $ident;

        return $this;

    }

    public function setPrefix($prefix): Logger {

        $this->prefix = (string) $prefix;

        return $this;

    }

    public function setOptions($options): Logger {

        $this->options = $options;

        return $this;

    }

    public function setDisplay($display): Logger {

        $this->display = (bool) $display;

        return $this;

    }

    public function setFacility($facility): Logger {

        $this->facility = $facility;

        return $this;

    }

    public function setPriority($priority): Logger {

        $this->priority = (int) $priority;

        return $this;

    }

    public function arbiter($priority): bool {

        return !(bool) ($this->priority < $priority);
    }

    public function write($priority, $message): bool {

        if (!$this->arbiter($priority)) 
            return false;

        $message = self::$priorities[$priority] . ' ' . $message;
     
        if ($this->arbiter(self::LOG_DEBUG)) {

            if(!isset(self::$debugMessages[self::$priorities[$priority]]))
                self::$debugMessages[self::$priorities[$priority]] = [];

            self::$debugMessages[self::$priorities[$priority]][] = $message; 

        }

        if($this->display)
            echo $message;

        openlog($this->ident . ' ' . $this->prefix, $this->options, (int) $this->facility);

        syslog($priority, $message);

        closelog();

        return true;

    }

	static public function getPool(string $name = self::DEFAULT_LOGGER): Logger {

		if(!isset(self::$pool[$name]) && $name != self::DEFAULT_LOGGER)
			throw new \Exception('can not find any logger "'.$name.'" in the logger pool');

		if(!isset(self::$pool[$name]))
			self::$pool[$name] = new Logger();

		return self::$pool[$name];

	}

	static public function setPool(Logger $database, string $name = self::DEFAULT_LOGGER): void {

		self::$pool[$name] = $database;

	}

    static public function debug(string $message): bool {

        return self::getPool()->write(self::LOG_DEBUG, $message);

    }

    static public function info(string $message): bool {

        return self::getPool()->write(self::LOG_INFO, $message);

    }

    static public function notice(string $message): bool {

        return self::getPool()->write(self::LOG_NOTICE, $message);

    }

    static public function warning(string $message): bool {

        return self::getPool()->write(self::LOG_WARNING, $message);

    }

    static public function error(string $message): bool {

        return self::getPool()->write(self::LOG_ERR, $message);

    }

    static public function critical(string $message): bool {

        return self::getPool()->write(self::LOG_CRIT, $message);

    }

    static public function alert(string $message): bool {

        return self::getPool()->write(self::LOG_ALERT, $message);

    }

    static public function emerg(string $message): bool {

        return self::getPool()->write(self::LOG_EMERG, $message);

    }

}