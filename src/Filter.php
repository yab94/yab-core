<?php

namespace Yab\Core;

use Yab\Core\Statement;
use Yab\Core\Tool;
use Yab\Core\View;
use Yab\Core\Form;

class Filter extends Form {

    protected $request = null;
    protected $response = null;
    protected $prefix = null;
    
    public function __construct(Request $request, Response $response, $prefix = '') {

        $this->request = $request;
        $this->response = $response;

        $this->prefix = (string) $prefix;

        $this->setAttributes(['method' => 'GET']);

    }

    public function getResetUrl() {

        $params = [];

        foreach($this->fields as $field) 
            $params[$field->getName()] = '';

        $request = clone $this->request;

        $request->addQueryParams($params);

        return $request->getUri();

    }

    protected function processRequest() {

        if(!$this->prefix)
            return parent::process($this->request);
        
        $prefix = $this->prefix;

        $cookies = $this->request->getCookies();

        $keys = array_keys($cookies);

        $keys = array_map(function($key) use($prefix) {
            return preg_replace('#^'.preg_quote($prefix, '#').'#', '', $key);
        }, $keys);

        $cookies = array_combine($keys, $cookies);
   
        $this->setValues($cookies);

        return parent::process($this->request);

    }

    public function filter(Statement $statement) {

        $this->processRequest();

        array_map(function($field) use($statement) {

            if(in_array($field->getAttribute('type'), ['button', 'submit'])) 
                return;

            if($this->prefix) 
                $this->response->setCookie($this->prefix.$field->getName(), $field->getValue());

            if(!$field->getValue()) 
                return;
 
            if(isset($field->callback)) {
                $callback = $field->callback;
                return $callback($statement, $field->getValue());
            }

            if(!isset($field->filter)) 
                $field->filter = $field->getName();

            if(!is_array($field->filter))
                return $statement->whereEq($field->filter, $field->getValue());

            foreach($field->filter as $column)
                $statement->orWhereLike($column, $field->getValue());

        }, $this->getFields());
        
    }

}