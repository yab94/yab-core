<?php

namespace Yab\Core;

abstract class Controller {

    use Tool;
    
    public function __construct(Request $request, Response $response) {

        $this->request = $request;
        $this->response = $response;

        if($this->getFlashMessages() && !$this->request->isAjax()) 
            $this->clearFlashMessages();
       
    }

    public function forward(string $controllerAction, array $params = [], array $queryParams = [], string $anchor = null): void {

        if(strpos($controllerAction, '.') === false)
            $controllerAction = get_class($this).'.'.$controllerAction;

        $this->redirect($this->resolve('GET', $controllerAction, $params, $queryParams, $anchor));

    }

    public function redirect($url): void {
		
        $this->response->redirect($url)->send();

    }

    public function getFlashMessages() {

        $cookieJson =  $this->request->getCookie(Config::get('application.flashMessageVar', 'FLASH_MESSAGE_VAR'), '[]');
    
        $flashMessages = json_decode($cookieJson, true);

        return $flashMessages;
    }

    public function clearFlashMessages() {

        return $this->response->setCookie(Config::get('application.flashMessageVar', 'FLASH_MESSAGE_VAR'), '[]');
    
    }

    public function addFlashMessage(string $message, string $type = 'success') {

        $cookieJson = $this->response->getCookie(Config::get('application.flashMessageVar', 'FLASH_MESSAGE_VAR'), '[]');

        $cookie = json_decode($cookieJson, true);
        $cookie[$type] = $cookie[$type] ?? [];
        $cookie[$type][] = $message;
        $cookieJson = json_encode($cookie);

        return $this->response->setCookie(Config::get('application.flashMessageVar', 'FLASH_MESSAGE_VAR'), $cookieJson);
    
    }

    protected function render($template = null, array $vars = [], bool $withLayout = null) {

        $context = [
            'request' => $this->request,
            'response' => $this->response,
            'routeClosure' => $this->request->getUri(),
            'flashMessages' => $this->getFlashMessages(),
            'baseUrl' => Config::get('application.baseUrl', ''),
        ];

        if(!($template instanceof View)) 
            $template = (new View())->setTemplate($template);

        $template->setVars($context)->setVars($vars);

        if($withLayout === null)
            $withLayout = !$this->request->isAjax();

		if(!$withLayout) {

            Config::set('debug.clients', '');

            $this->response->appendBody($template->getRender());

            return $this;
            
        }

        $layout = (new View())->setTemplate(Config::get('application.layout'))->setVars($context)->setVars(['view' => $template] + $vars);

		$this->response->appendBody($layout->getRender());

        return $this;
    }

    public function before() {}

    public function after() {}
 
}