<?php

namespace Yab\Core;

class Form extends View {

    protected $template = 'templates/form.php';

    protected $fields = [];

    protected function onRender(): View {

        $this->set('fields', array_map(function($fields) { return $fields->onRender(); }, $this->fields));
        $this->set('errors', $this->getErrors());
        
        return $this;

    }

    public function setValues($values): Form {

        if($values instanceof Model)
            $values = $values->getAttributes();

        foreach($this->fields as $name => $field) {

            if(!$field->isValuable())
                continue;

            if(!isset($values[$name]))
                continue;

            $field->setValue($values[$name]);

        }

        return $this;

    }

    public function getErrors(): array {

        return array_map(function($field) { return $field->getErrors(); }, $this->fields);

    }
    
    public function getFields(): array {

        return $this->fields;

    }

    public function getField($name): Field {
        
        if(!isset($this->fields[$name]))
            throw new \Exception('unknown form field "'.$name.'"');

        return $this->fields[$name];
    }

    public function getValues(): array {

        return array_map(function($field) { 
            
            return $field->getValue(); 

        }, array_filter($this->fields, function($field) { 

            return $field->isValuable(); 
            
        }));

    }

    public function getValue($name) {

        return $this->getField($name)->getValue();

    }

    public function getRequestValue(Request $request, Field $field) {

        if(in_array($field->getAttribute('type'), ['file'])) 
            return $request->getFileParam($field->getName());

        if(strtoupper($this->getAttribute('method')) == 'GET')
            return $request->getQueryParam($field->getName());

        return $request->getBodyParam($field->getName());

    }

    public function process(Request $request): bool {

        if(strtoupper($this->getAttribute('method')) != strtoupper($request->getVerb()))
            return false;

        $process = true;

        foreach ($this->fields as $field) {
                
            if(!$field->isValidable())
                continue;

            try {

                $value = $this->getRequestValue($request, $field);

            } catch(\Exception $e) {
                
                $value = null;

            }

            if(!$field->process($value)) {

                Logger::debug('field '.$field->getName().' process KO');

                $process = false;

            }

        }

        return $process;
    }

    public function addField($name): Field {

        $this->fields[$name] = new Field($name);

        return $this->fields[$name];
    }

    public function addText($name): Field {

        return $this->addField($name)->setAttribute('type', 'text')->setTemplate('templates/form/text.php');

    }

    public function addFile($name): Field {

        $this->setAttribute('enctype', 'multipart/form-data');

        return $this->addField($name)->setAttribute('type', 'file')->setTemplate('templates/form/file.php');

    }

    public function addTextarea($name): Field {

        return $this->addField($name)->setTemplate('templates/form/textarea.php');

    }

    public function addSelect($name): Field {

        return $this->addField($name)->setTemplate('templates/form/select.php');

    }

    public function addRadio($name): Field {

        return $this->addField($name)->setAttribute('type', 'radio')->setTemplate('templates/form/radio.php');

    }

    public function addPassword($name): Field {

        return $this->addField($name)->setAttribute('type', 'password')->setTemplate('templates/form/password.php');

    }

    public function addSubmit($value, $name = 'submit'): Field {

        return $this->addField($name)->setValue($value)->setValidable(false)->setValuable(false)->setAttribute('type', 'submit')->setTemplate('templates/form/submit.php');

    }

    public function addHidden($name, $value): Field {

        return $this->addField($name)->setValue($value)->setAttribute('type', 'hidden')->setTemplate('templates/form/hidden.php');

    }

    public function addCheckbox($name): Field {

        return $this->addField($name)->setAttribute('type', 'checkbox')->setTemplate('templates/form/checkbox.php');

    }

}

// Do not clause PHP tags unless it is really necessary