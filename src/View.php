<?php
 
namespace Yab\Core;

require_once 'Tool.php';

use Yab\Core\Tool;

class View { 

    use Tool;
    
    static protected $blocks = [];

    protected $openedBlock = null;
    
    protected $buffer = null;

    protected $excludePaths = [];
    
    protected $template = null;
    protected $vars = [
        'attributes' => [],
    ];

    final protected function partial($template, array $vars = []): View {

        $partial = clone $this;

        $partial->setTemplate($template);
        $partial->setVars($vars);

        return $partial;
        
    }

    final public function parent(array $vars = []) {

        $parent = clone $this;

        $parent->excludePath($this->findPath());
        $parent->setTemplate($this->template);
        $parent->setVars($vars);

        return $parent;
    }
    
    public function setAttributes(array $attributes): View {

        foreach($attributes as $name => $value)
            $this->setAttribute($name, $value);

        return $this;

    }
    
    public function appendAttribute($name, $value): View {

        return $this->setAttribute($name, trim($this->getAttribute($name).' '.$value));

    }
    
    public function prependAttribute($name, $value): View {

        return $this->setAttribute($name, trim($value.' '.$this->getAttribute($name)));

    }
    
    public function setHref($controllerAction, array $params = [], array $queryParams = [], $method = 'GET'): View {

        return $this->setAttribute('href', Tool::resolve($method, $controllerAction, $params, $queryParams));

    }
    
    public function setAttribute($name, $value): View {

        if(!isset($this->vars['attributes']))
            $this->vars['attributes'] = [];
        
        $this->vars['attributes'][$name] = $value;

        return $this;

    }
    
    public function getAttribute($name, $defaultValue = null) {

        return isset($this->vars['attributes'][$name]) ? $this->vars['attributes'][$name] : $defaultValue;

    }

    protected function onRender(): View { return $this; }
    
    public function __toString() {

        return $this->getRender();

    }

    final public function __set($name, $value) {
        
        return $this->set($name, $value);
        
    }
    
    final public function __get($name) {
        
        return $this->get($name);
        
    }
    
    final public function __isset($name) {
        
        return $this->has($name);
        
    }
    
    final public function __unset($name) {
        
        return $this->unset($name);
        
    }

    public function setVars(array $vars): View {

        foreach($vars as $name => $value)
            $this->set($name, $value);

        return $this;

    }
    
    final public function get($name) {
        
        if(!$this->has($name)) 
            throw new \Exception('unable to find var "'.$name.'"');

        return $this->vars[$name];
        
    }
    
    final public function set($name, $value): View {
        
        $this->vars[$name] = $value;
        
        return $this;
        
    }
    
    final public function unset($name): View {
        
        if(!$this->has($name))
            throw new \Exception('unable to find var "'.$name.'"');
        
        unset($this->vars[$name]);

        return $this;
        
    }
    
    final public function has($name): bool {
        
        return array_key_exists($name, $this->vars);
        
    }
    
    final public function setTemplate($template): View {

        $this->template = (string) $template;
        
        return $this;
        
    }
    
    final public function getTemplate() {
        
        return $this->template;
        
    }

    final protected function openBlock($block): View {

		ob_start();
        
        $this->openedBlock = (string) $block;
        
        return $this;

    }

    final protected function closeBlock(): View {

		$block = ob_get_clean();
        
        if(!isset(self::$blocks[$this->openedBlock]))
            self::$blocks[$this->openedBlock] = '';
        
        self::$blocks[$this->openedBlock] .= (string) $block;
        
        $this->openedBlock = null;
        
        return $this;

    }

    final static protected function blockExists($block): bool {
        
        return isset(self::$blocks[$block]);

    }

    final public function getBlockRender($block, $defaultBody = '') {

        # Si on demande l'affiche d'un block alors que la view/layout n'ont meme pas été généré, il faut lancer le calcul de la view/layout
        if($this->buffer === null) {
            
            Logger::debug('PRE RENDERING FOR BLOCK "' . $block.'"');
            
            $this->getRender();
            
        }

		return isset(self::$blocks[$block]) ? self::$blocks[$block] : $defaultBody;

    }

    final public function getRender(): string {

        if($this->buffer !== null)
            return $this->buffer;
        
		ob_start();
		
		try {
			
			$this->render();
			
		} catch(\Exception $e) {
            
            if(0 < ob_get_level())
                ob_end_clean();

			throw $e;
			
		}
		
        $this->buffer = (0 < ob_get_level()) ? (string) ob_get_clean() : '';
        
        return $this->buffer;

    }

    final public function excludePath($path): View {

        $this->excludePaths[] = $path;

        return $this;

    }

    final public function getPaths(): array {

        return explode(PATH_SEPARATOR, get_include_path());

    }

    final public function findPath(): string {

        if(!$this->template)
            throw new \Exception('can not find path with no template setted');

        foreach ($this->getPaths() as $path) {

            $filePath = $path . DIRECTORY_SEPARATOR . $this->template;

            if(in_array($path, $this->excludePaths))
                continue;

            if(file_exists($filePath))
                return $path;

        }

        return '';

    }
    
    final public function render(array $vars = []): View {
     
        if($this->buffer !== null) {
            
            echo $this->buffer;
            
            return $this;
            
        }

        if(!$this->template)
            throw new \Exception('can not render with no template setted');

        $this->onRender();

        extract(get_object_vars($this), EXTR_OVERWRITE);
        extract($this->vars['attributes'], EXTR_OVERWRITE);
        extract($this->vars, EXTR_OVERWRITE);
        extract($vars, EXTR_OVERWRITE);

        $path = $this->findPath();

		include ($path ? ($path . DIRECTORY_SEPARATOR) : '') . $this->template;

        return $this;
            
    }
    
    final protected function renderBlock($block, $defaultBody = '') {
        
        echo $this->getBlockRender($block, $defaultBody);
        
    }
 
}

// Do not clause PHP tags unless it is really necessary