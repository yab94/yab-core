<?php

namespace Yab\Core;

trait Http {

    static public $verbs = [
        'GET',
        'POST',
        'PUT',
        'DELETE',
        'PATCH',
        'OPTIONS',
        'HEAD',
    ];

    static public $codes = [
        100 => "Continue", 
        101 => "Switching Protocols", 
        102 => "Processing", 
        200 => "Ok", 
        201 => "Created", 
        202 => "Accepted", 
        203 => "Non-Authoritative Information", 
        204 => "No Content", 
        205 => "Reset Content", 
        206 => "Partial Content", 
        207 => "Multi-Status", 
        300 => "Multiple Choices", 
        301 => "Moved Permanently", 
        302 => "Found", 
        303 => "See Other", 
        304 => "Not Modified", 
        305 => "Use Proxy", 
        306 => "(Unused)", 
        307 => "Temporary Redirect", 
        308 => "Permanent Redirect", 
        400 => "Bad Request", 
        401 => "Unauthorized", 
        402 => "Payment Required", 
        403 => "Forbidden", 
        404 => "Not Found", 
        405 => "Method Not Allowed", 
        406 => "Not Acceptable", 
        407 => "Proxy Authentication Required", 
        408 => "Request Timeout", 
        409 => "Conflict", 
        410 => "Gone", 
        411 => "Length Required", 
        412 => "Precondition Failed", 
        413 => "Request Entity Too Large", 
        414 => "Request-URI Too Long", 
        415 => "Unsupported Media Type", 
        416 => "Requested Range Not Satisfiable", 
        417 => "Expectation Failed", 
        418 => "I'm a teapot", 
        419 => "Authentication Timeout", 
        420 => "Enhance Your Calm", 
        422 => "Unprocessable Entity", 
        423 => "Locked", 
        424 => "Failed Dependency", 
        424 => "Method Failure", 
        425 => "Unordered Collection", 
        426 => "Upgrade Required", 
        428 => "Precondition Required", 
        429 => "Too Many Requests", 
        431 => "Request Header Fields Too Large", 
        444 => "No Response", 
        449 => "Retry With", 
        450 => "Blocked by Windows Parental Controls", 
        451 => "Unavailable For Legal Reasons", 
        494 => "Request Header Too Large", 
        495 => "Cert Error", 
        496 => "No Cert", 
        497 => "HTTP to HTTPS", 
        499 => "Client Closed Request", 
        500 => "Internal Server Error", 
        501 => "Not Implemented", 
        502 => "Bad Gateway", 
        503 => "Service Unavailable", 
        504 => "Gateway Timeout", 
        505 => "HTTP Version Not Supported", 
        506 => "Variant Also Negotiates", 
        507 => "Insufficient Storage", 
        508 => "Loop Detected", 
        509 => "Bandwidth Limit Exceeded", 
        510 => "Not Extended", 
        511 => "Network Authentication Required", 
        598 => "Network read timeout error", 
        599 => "Network connect timeout error",
    ];

    protected $protocol = "HTTP/1.0";
    protected $body = '';
    protected $headers = [];

    static public function verbs() {

        return self::$verbs;
        
    }

    static public function unparseUrl(array $parsedUrl): string {

        $scheme   = isset($parsedUrl['scheme']) ? $parsedUrl['scheme'] . '://' : '';
        $host     = isset($parsedUrl['host']) ? $parsedUrl['host'] : '';
        $port     = isset($parsedUrl['port']) ? ':' . $parsedUrl['port'] : '';
        $user     = isset($parsedUrl['user']) ? $parsedUrl['user'] : '';
        $pass     = isset($parsedUrl['pass']) ? ':' . $parsedUrl['pass']  : '';

        $pass     = ($user || $pass) ? "$pass@" : '';

        $path     = isset($parsedUrl['path']) ? $parsedUrl['path'] : '';
        $query    = isset($parsedUrl['query']) ? '?' . $parsedUrl['query'] : '';
        $fragment = isset($parsedUrl['fragment']) ? '#' . $parsedUrl['fragment'] : '';

        return "$scheme$user$pass$host$port$path$query$fragment";

    }

    static public function formatHeaderName($headerName) {

        return ucwords(strtolower(trim($headerName)), '-');

    }

    static public function codeByMessage(string $camelCaseMessage): int {

        foreach(self::$codes as $code => $message) 
            if(Tool::camelCase($message) == $camelCaseMessage)
                return $code;

        throw new \Exception('invalid HTTP camelCase message "'.$camelCaseMessage.'"', 500);

    }

    static public function messageByCode(int $code): string {

        if(!isset(self::$codes[$code]))
            throw new \Exception('invalid HTTP code "'.$code.'"', 500);

        return self::$codes[$code];

    }

    static public function multiu(int $code): string {

        if(!isset(self::$codes[$code]))
            throw new \Exception('invalid HTTP code "'.$code.'"', 500);

        return self::$codes[$code];

    }

    static public function multipartFormDataParams(array $params, $boundary) {
        $body = '';
        foreach($params as $name => $value) {
            $body .= $boundary.PHP_EOL.'Content-Disposition: form-data; name="'.$name.'"'.PHP_EOL.PHP_EOL.$value.PHP_EOL;
        }
        return $body;
    }

    public function setProtocol(string $protocol): self {

        $this->protocol = (string) $protocol;

        return $this;

    }

    public function getProtocol(): string {

        return $this->protocol;

    }

    public function setHeaders(array $headers): self {

        foreach($headers as $headerName => $headerValue)
            $this->setHeader($headerName, $headerValue);

        return $this;

    }

    public function addHeaders(array $headers): self {

        foreach($headers as $headerName => $headerValue)
            $this->addHeader($headerName, $headerValue);

        return $this;

    }

    public function setHeader(string $headerName, $headerValue): self {

        $headerName = self::formatHeaderName($headerName);
        
        $this->headers[$headerName] = array_unique(is_array($headerValue) ? $headerValue : [$headerValue]);

        return $this;

    }

    public function addHeader(string $headerName, $headerValue): self {

        try {

            $headerValues = $this->getHeaders($headerName);

            $headerValues[] = $headerValue;

            return $this->setHeader($headerName, $headerValues);

        } catch(\Exception $e) {

            return $this->setHeader($headerName, $headerValue);

        }

    }

    public function getHeaders(string $headerName = null): array {

        if($headerName === null)
            return $this->headers;

        $headerName = self::formatHeaderName($headerName);

        if(!isset($this->headers[$headerName]))
            throw new \Exception('missing header "'.$headerName.'"', 400);

        return $this->headers[$headerName];

    }

    public function getHeader(string $headerName, string $defaultValue = null): string {

        try {

            $headers = $this->getHeaders($headerName);

            return trim($headers[0]);

        } catch(\Exception $e) {

            if($defaultValue === null)
                throw $e;

        }

        return $defaultValue;

    }

    public function getBodyParam(string $name, $defaultValue = null) {

        $params = $this->getBodyParams();

        if(!isset($params[$name]) && $defaultValue === null)
            throw new \Exception('invalid body param "'.$name.'"', 400);
        
        return $params[$name] ?? $defaultValue;

    }     

    public function addBodyParam(string $param, $value): self {

        $params = $this->getBodyParams();

        $params[$param] = $value;

        return $this->setBodyParams($params);

    }

    public function removeBodyParam(string $param): self {

        $params = $this->getBodyParams();

        if(isset($params[$param]))
            unset($params[$param]);

        return $this->setBodyParams($params);

    }

    public function isMultipartFormData() {
        return strpos($this->getHeader('Content-Type', ''), 'multipart/form-data') === 0;
    }

    public function getMultipartFormDataBoundary() {
        return preg_match('#boundary=([\-_a-zA-Z0-9]+)#is', $this->getHeader('Content-Type', ''), $match) ? $match[1] : "";
    }

    public function getMultipartFormDataParams() {
        $body = $this->getBody();
        $boundary = $this->getMultipartFormDataBoundary();
        $params = [];
        $parts = explode($boundary, $body);
        $parts = array_filter($parts);
        foreach($parts as $part) {
            $param = $this->getMultipartFormDataParam($part);
            if(!$param) {
                continue;
            }
            $params[$param['name']] = $param['value'];
        }
        return $params;
    }

    protected function getMultipartFormDataParam($part) {
        $lines = preg_split('#\r\n|\r|\n#s', trim($part));
        $name = null;
        $value = "";
        $head = true;
        foreach($lines as $line) {
            if(trim($line) === "") {
                $head = false;
                continue;
            }
            if($head && preg_match('#name="([^"]+)"#', $line, $match)) {
                $name = $match[1];
            }
            if(!$head) {
                $value .= $line;
            }
        }
        return $name ? ['name' => $name, 'value' => $value] : null;
    }

    protected function setMultipartFormDataParams(array $params) {
        $boundary = $this->getMultipartFormDataBoundary();
        $body = '';
        foreach($params as $name => $value) {
            $body .= $boundary.PHP_EOL.'Content-Disposition: form-data; name="'.$name.'"'.PHP_EOL.PHP_EOL.$value.PHP_EOL;
        }
        return $body;
    }
    
    public function getBodyParams(): array {

        $contentType = $this->getHeader('Content-Type', '');

        $params = [];

        if(strpos($contentType, 'application/json') === 0) {
            $params = json_decode($this->getBody(), true); 
        } elseif($this->isMultipartFormData()) {
            $params = $this->getMultipartFormDataParams();
        } else {
            parse_str($this->getBody(), $params); 
        }

        if(!$params)
            $params = [];

        return $params;

    }  

    public function setBodyParams(array $params): self {
        $contentType = $this->getHeader('Content-Type', '');
        if(strpos($contentType, 'application/json') === 0) {
            $this->setBody(json_encode($params)); 
        } elseif($this->isMultipartFormData()) {
            $this->setBody(self::multipartFormDataParams($params, $this->getMultipartFormDataBoundary())); 
        } else {
            $this->setBody(http_build_query($params)); 
        }
        return $this;
    }

    public function appendBody(string $body): self {

        $this->body .= $body;

        return $this;

    }

    public function setBody(string $body): self {

        $this->body = $body;

        return $this;

    }
    
    public function getBody(): string {

        return $this->body;

    }

}