<?php

namespace Yab\Core;

class Cache {

    static protected $keys = 0;
    static protected $cache = [];

    static public function getUniqueKey(): string {

        return ++self::$keys;
        
    }

    static public function get($key, $defaultValue = null) {

        return self::isset($key) ? self::$cache[$key] : $defaultValue;

    }

    static public function set(string $key, $value): void {

        self::$cache[$key] = $value;

    }

    static public function isset($key): bool {

        return isset(self::$cache[$key]);

    }

    static public function unset($key): void {

        unset(self::$cache[$key]);

    }

    static public function cache($value): string {

        $uniqueKey = self::getUniqueKey();

        self::set($uniqueKey, $value);

        return $uniqueKey;

    }

}