<?php

$response->setHeader('Content-Type', 'text/csv');
$response->setHeader('Content-Disposition', 'attachment; filename="'.$filename.'"');

$response->flush();

$fh = fopen('php://output', 'w+');

foreach($datas as $line) 
    fputcsv($fh, $line, ';', '"', "\\");

fclose($fh);

$response->send();