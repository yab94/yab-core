<table>
    <thead>
        <tr>
            <?php foreach($headers as $header): ?>
            <th><?php echo $header; ?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach($datas as $line): ?>
        <tr>
            <?php foreach($line as $col): ?>  
            <td><?php echo $col; ?></td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>