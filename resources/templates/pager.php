<?php if(!$displayPages): return; endif; ?>
<div class="btn-group">
    <a class="btn btn-primary<?php if($page == $firstPage): ?> disabled<?php endif; ?>" href="<?php echo $firstPageUrl; ?>">&lt;&lt;</a>
    <a class="btn btn-primary<?php if($page == $previousPage): ?> disabled<?php endif; ?>" href="<?php echo $previousPageUrl; ?>">&lt;</a>
    <?php for($i = $page - $displayPages; $i < $page; $i++): ?>
        <?php if(0 < $i): ?>
            <a class="btn btn-primary" href="<?php echo $getPageUrl($i); ?>"><?php echo $i; ?></a>
        <?php endif; ?>
    <?php endfor; ?> 
    <a class="btn btn-primary disabled" href="#"><?php echo $page; ?></a>
    <?php for($i = $page + 1; $i < $page + $displayPages; $i++): ?>
        <?php if($i < $lastPage): ?>
            <a class="btn btn-primary" href="<?php echo $getPageUrl($i); ?>"><?php echo $i; ?></a>
        <?php endif; ?>
    <?php endfor; ?> 
    <a class="btn btn-primary<?php if($page == $nextPage): ?> disabled<?php endif; ?>" href="<?php echo $nextPageUrl; ?>">&gt;</a>
    <a class="btn btn-primary<?php if($page == $lastPage): ?> disabled<?php endif; ?>" href="<?php echo $lastPageUrl; ?>">&gt;&gt;</a>
    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Afficher
    </button>
    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
        <?php foreach($perPages as $perPage): ?>
        <a class="dropdown-item" href="<?php echo $getPerPageUrl($perPage); ?>"><?php echo $perPage == $rowCount ? 'les '.$rowCount : 'par '.$perPage ; ?></a>
        <?php endforeach; ?>
    </div>
</div>