<?php if(isset($dumpTitle)): ?><li><?php echo $dumpTitle; ?></li><?php endif; ?>
<ul>
<?php foreach($data as $key => $value): ?>
    <?php if(is_array($value)): ?>
    <?php echo $this->partial('templates/dump.php', array('dumpTitle' => $key, 'data' => $value)); ?>
    <?php elseif(is_object($value)): ?>
    <li><?php echo $key; ?>: <?php print_r($value); ?></li>
    <?php else: ?>
    <li><?php echo $key; ?>: <?php echo $value; ?></li>
    <?php endif; ?>
<?php endforeach; ?>
</ul>