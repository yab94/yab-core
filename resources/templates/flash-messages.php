<?php foreach($flashMessages as $type => $messages): ?>
    <?php foreach($messages as $message): ?>
    <div class="alert alert-<?php echo $type; ?>" role="alert">
        <?php echo $this->html($message); ?>
    </div>
    <?php endforeach; ?>
<?php endforeach; ?>