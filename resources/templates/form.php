<form <?php echo $this->htmlAttributes($attributes); ?>>
    <fieldset>
        <?php if(isset($title)): ?><legend><?php echo $this->html($title); ?></legend><?php endif; ?> 
        <?php if(isset($errors)): ?>
        <?php foreach($errors as $messages): ?>
            <?php foreach($messages as $message): ?>
            <p class="error"><?php echo $this->html($message); ?></p>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <?php endif; ?>
        <?php foreach($fields as $field): ?>
            <?php if(in_array($field->getAttribute('type'), array('submit', 'button'))) continue; ?>
			<div class="form-group">
				<label for="<?php echo $field->getAttribute('id') ?? $field->getName(); ?>"><?php echo $this->html($field->label); ?></label>
				<?php $field->render(); ?>
			</div>
        <?php endforeach; ?>
            <p class="button">
        <?php foreach($fields as $field): ?>
            <?php if(!in_array($field->getAttribute('type'), array('submit', 'button'))) continue; ?>
            <?php $field->render(); ?>
        <?php endforeach; ?>
            </p>
    </fieldset>
</form>
