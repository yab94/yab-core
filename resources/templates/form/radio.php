<?php foreach($options as $optionKey => $optionValue): ?>
	<?php $attributes['id'] = $name.'_'.$optionKey; ?>
    <div class="form-check">
		<input name="<?php echo $name; ?>" value="<?php echo $this->html($optionKey); ?>" <?php echo $this->htmlAttributes($attributes + ($optionKey == $value ? ['checked' => 'checked'] : [])); ?> />
		<label for="<?php echo $attributes['id']; ?>"><?php echo $this->html($optionValue); ?></label>
	</div>
<?php endforeach; ?>
