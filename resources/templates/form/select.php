<select name="<?php echo $name; ?>"<?php echo $this->htmlAttributes($attributes); ?>>
<?php foreach($options as $optionKey => $optionValue): ?>
    <option value="<?php echo $this->html($optionKey); ?>"<?php if($optionKey == $value): ?> selected="selected"<?php endif; ?>><?php echo $this->html($optionValue); ?></option>
<?php endforeach; ?>
</select>

